from django.contrib.gis.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from django.utils import timezone
from main.icon_helpers import make_pin, make_color
from django.core.files import File
from django.contrib.auth import get_user_model
import markdown
import pytz
from datetime import datetime

User  = get_user_model()


class GeoContext(models.Model):
    label = models.CharField(max_length=200, unique=True)
    description = models.TextField(blank=True, default="")
    
    country_code = models.CharField(max_length=4, blank=True, default="")
    monument_definition = models.CharField(max_length=100, blank=True, default="Monument")
    app_domain = models.CharField(max_length=200, blank=True, default="app.wikilovesmonuments.it")
    language_code = models.CharField(max_length=10, blank=True, default="en")

    commons_category_label = models.CharField(max_length=200)

    flag = models.CharField(max_length=200, blank=True, default="")
    centroid = models.PointField(blank=True, null=True)
    zoom_level = models.FloatField(default=8.0)

    timezone_name = models.CharField(max_length=200, blank=True, default="Europe/Rome")

    regions_overpass_query = models.TextField(blank=True, default="")
    regions_name_tag = models.CharField(max_length=200, blank=True, default="name")

    provinces_overpass_query = models.TextField(blank=True, default="")
    provinces_name_tag = models.CharField(max_length=200, blank=True, default="name")

    municipalities_overpass_query = models.TextField(blank=True, default="")
    municipalities_name_tag = models.CharField(
        max_length=200, blank=True, default="name"
    )

    enable_donations_text = models.BooleanField(default=False, help_text="Enable donations text in the app")
    donations_popup_probability = models.IntegerField(
        help_text="Probability of showing the donations popup (0-100)",
        default=0, blank=True, validators=[MinValueValidator(0), MaxValueValidator(100)])

    def __str__(self):
        return self.label
    
    def clean(self):
        if self.timezone_name:
            try:
                tz = pytz.timezone(self.timezone_name)
            except pytz.exceptions.UnknownTimeZoneError:
                raise ValidationError(f"Timezone {self.timezone_name} not recognized")
       

    class Meta:
        ordering = ["label"]




class GeoContextAdmin(models.Model):
    geo_context = models.ForeignKey(GeoContext, models.CASCADE, related_name="admins")
    user = models.ForeignKey(User, models.CASCADE, related_name="admin_geo_contexts_set")

    class Meta:
        unique_together = ["geo_context", "user"]


class Icon(models.Model):
    geo_context = models.ForeignKey(GeoContext, models.CASCADE, related_name="icons")
    # todo: restrict to svg files
    name = models.CharField(max_length=200)
    icon = models.FileField(upload_to="icons", null=True, blank=True)

    icon_white = models.FileField(upload_to="icons", null=True, blank=True)
    icon_color = models.FileField(upload_to="icons", null=True, blank=True)
    icon_black = models.FileField(upload_to="icons", null=True, blank=True)

    icon_pin_many = models.FileField(upload_to="icons", null=True, blank=True)
    icon_pin_many_contest = models.FileField(upload_to="icons", null=True, blank=True)
    icon_pin_some = models.FileField(upload_to="icons", null=True, blank=True)
    icon_pin_some_contest = models.FileField(upload_to="icons", null=True, blank=True)
    icon_pin_none = models.FileField(upload_to="icons", null=True, blank=True)
    icon_pin_none_contest = models.FileField(upload_to="icons", null=True, blank=True)

    # todo: move to signals..
    def save(self, *args, **kwargs):
        icon = self.icon.file.read()
        
        black = make_color(icon, "black", "white")
        self.icon_black.save(f"{self.name}-black.png", File(black), save=False)

        white = make_color(icon, "white", "")
        self.icon_white.save(f"{self.name}-white.png", File(white), save=False)

        color = make_color(icon, "rgb(17, 138, 178)", "")
        self.icon_color.save(f"{self.name}-color.png", File(color), save=False)

        pin_many = make_pin(icon, "many", False)
        self.icon_pin_many.save(f"{self.name}-pin-many.png", File(pin_many), save=False)

        pin_many_contest = make_pin(icon, "many", True)
        self.icon_pin_many_contest.save(f"{self.name}-pin-many-contest.png", File(pin_many_contest), save=False)

        pin_some = make_pin(icon, "some", False)
        self.icon_pin_some.save(f"{self.name}-pin-some.png", File(pin_some), save=False)

        pin_some_contest = make_pin(icon, "some", True)
        self.icon_pin_some_contest.save(f"{self.name}-pin-some-contest.png", File(pin_some_contest), save=False)

        pin_none = make_pin(icon, "none", False)
        self.icon_pin_none.save(f"{self.name}-pin-none.png", File(pin_none), save=False)

        pin_none_contest = make_pin(icon, "none", True)
        self.icon_pin_none_contest.save(f"{self.name}-pin-none-contest.png", File(pin_none_contest), save=False)

        out = super().save(*args, **kwargs)


        return out
    
    def __str__(self):
        return self.name


class AppCategory(models.Model):
    """
    Categories for app. TODO: add icon
    """

    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="app_categories"
    )
    name = models.CharField(max_length=200)

    # settore per categorizzazione immagini su commons
    sector = models.CharField(max_length=200, blank=True, default="")

    order = models.IntegerField(default=0)
    priority = models.IntegerField(default=0)
    icon = models.ForeignKey(
        Icon, models.SET_NULL, null=True, blank=True, related_name="app_categories"
    )

    is_municipality = models.BooleanField(default=False)
    is_other_monuments = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name} ({self.geo_context})"

    class Meta:
        ordering = ["priority"]
        verbose_name_plural = "app categories"


class DataCategory(models.Model):
    """
    Categories for data.wikilovesmonuments
    """

    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="data_categories"
    )
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    priority = models.IntegerField(default=0)
    q_number = models.CharField(max_length=200, blank=True, null=True)
    group = models.CharField(max_length=200, blank=True, default="")

    # eventual mapping to app categories
    app_category = models.ForeignKey(
        AppCategory,
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="data_categories",
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["priority", "name"]
        verbose_name_plural = "data categories"


class Snapshot(models.Model):
    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="snapshots"
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    complete = models.BooleanField(default=False)
    killed = models.BooleanField(default=False)
    

    csv_export = models.FileField(upload_to="csv_exports", null=True, blank=True)
    xlsx_export = models.FileField(upload_to="xlsx_exports", null=True, blank=True)

    #id to hold async queue task id
    task_id = models.CharField(max_length=200, blank=True, default="")
    task_started_at = models.DateTimeField(blank=True, null=True)
    task_status = models.CharField(max_length=200, blank=True, default="")
    task_status_updated_at = models.DateTimeField(blank=True, null=True)
    task_error = models.TextField(blank=True, default="")

    #
    total_monuments = models.IntegerField(default=0)
    done_monuments = models.IntegerField(default=0)

    perform_commons = models.BooleanField(default=True)

    

    def __str__(self):
        return f"{self.created}"


class WLMCategoryRule(models.Model):
    # TODO: understand if use a more "hardcoded" approach for this model:
    # instance_of = models.CharField(max_length=200, blank=True, default="")
    # approved_by = models.CharField(max_length=200, blank=True, default="")
    # endorsed_by = models.CharField(max_length=200, blank=True, default="")

    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="wlm_category_rules"
    )

    app_category = models.ForeignKey(
        AppCategory, models.CASCADE, related_name="wlm_category_rules"
    )
    order = models.IntegerField(default=0, unique=True)

    def matches(self, dict_data):
        for predicate in self.predicates.all():
            datum = dict_data.get(predicate.property, '')
            #print(predicate.property, predicate.value, datum, type(datum))
            if type(datum) == list:
                if predicate.value not in datum:
                    return False
            else: 
                if str(dict_data.get(predicate.property, '')) != predicate.value:
                    return False
            return True
        return True

    class Meta:
        ordering = ["order"]


class WLMCategoryRulePredicate(models.Model):

    PROPERTY_CHOICES = [
        ("mon", "mon"),
        ("commonsCat", "commonsCat"),
        ("wlm", "wlm"),
        ("instanceOf_n", "instanceOf_n (P31)"),
        ("parent_n", "parent_n"),
        ("place_n", "place_n"),
        ("approvedBy_n", "approvedBy_n (P790)"),
        ("endorsedBy_n", "endorsedBy_n (P8001)"),
        ("accreditedBy_n", "accreditedBy_n (P5514)"),
        ("adminEntity", "adminEntity"),
        ("location", "location"),
    ]


    property = models.CharField(max_length=200, choices=PROPERTY_CHOICES)
    value = models.CharField(max_length=200)
    rule = models.ForeignKey(WLMCategoryRule, models.CASCADE, related_name="predicates")


    def __str__(self):
        return f"{self.get_property_display()} {self.value}"


class QueryPlaceholder(models.Model):
    """
    Placeholders for queries.
    Will be used to replace placeholders in the query.
    """

    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="query_constants"
    )
    symbol = models.CharField(max_length=200)

    class Meta:
        unique_together = ["geo_context", "symbol"]

    def __str__(self):
        return self.symbol


class QueryPlaceholderValue(models.Model):
    """
    Placeholder values for queries.
    Will be used to replace placeholders in the query.
    """

    placeholder = models.ForeignKey(
        QueryPlaceholder, models.CASCADE, related_name="values"
    )
    value = models.CharField(max_length=200)
    comment = models.TextField(blank=True, default="")

    class Meta:
        unique_together = ["placeholder", "value"]
        ordering = ["placeholder", "comment"]

    def __str__(self):
        return self.value


class Query(models.Model):
    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="sparql_queries"
    )
    label = models.CharField(max_length=200)
    sparql = models.TextField()
    description = models.TextField(blank=True, default="")

    # this flag is used to determine if monuments should be categorized or not (via WLMCategoryRule model)
    categorize_for_app = models.BooleanField(default=False)

    # fixed categories for data.wikilovesmonuments
    data_categories = models.ManyToManyField(DataCategory, blank=True)

    placeholder = models.ForeignKey(
        QueryPlaceholder, models.SET_NULL, null=True, blank=True, related_name="queries"
    )

    def __str__(self):
        return self.label
    
    class Meta:
        verbose_name_plural = "queries"


class QuerySnapshot(models.Model):
    snapshot = models.ForeignKey(
        Snapshot, models.CASCADE, related_name="query_snapshots"
    )
    query = models.ForeignKey(Query, models.CASCADE, related_name="query_snapshots")
    #holds the raw result of the query
    payload = models.JSONField(null=True, default=None, editable=False)
    
    total_monuments = models.IntegerField(default=0)
    done_monuments = models.IntegerField(default=0)
    
    complete = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    #id to hold async queue task id
    task_status = models.CharField(max_length=200, blank=True, default="")
    

    def has_payload(self):
        return bool(self.payload)

    def __str__(self):
        return self.query.label


# Geographical models


class Region(models.Model):
    geo_context = models.ForeignKey(GeoContext, models.CASCADE, related_name="regions")
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=200, blank=True, default="")
    poly = models.MultiPolygonField(blank=True, null=True)
    centroid = models.PointField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Province(models.Model):
    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="provinces"
    )
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=200, blank=True, default="")
    poly = models.MultiPolygonField(blank=True, null=True)
    centroid = models.PointField(blank=True, null=True)

    region = models.ForeignKey(
        Region, models.SET_NULL, blank=True, null=True, related_name="provinces"
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Municipality(models.Model):
    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="municipalities"
    )
    name = models.CharField(max_length=200)
    # code must be populated for this level, ands hould be a q number (will be used to join municipalities from scraping)
    code = models.CharField(max_length=200)
    poly = models.MultiPolygonField(blank=True, null=True)
    centroid = models.PointField(blank=True, null=True)

    province = models.ForeignKey(
        Province, models.SET_NULL, blank=True, null=True, related_name="municipalities"
    )
    region = models.ForeignKey(
        Region, models.SET_NULL, blank=True, null=True, related_name="municipalities"
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = "municipalities"


class Monument(models.Model):
    geo_context = models.ForeignKey(
        GeoContext, models.CASCADE, related_name="monuments"
    )
    label = models.TextField()
    q_number = models.CharField(max_length=200, unique=True)
    parent_q_number = models.CharField(max_length=200, blank=True, default="")
    wlm_n = models.CharField(max_length=200, blank=True, default="")
    start = models.DateTimeField(blank=True, null=True, db_index=True)
    end = models.DateTimeField(blank=True, null=True, db_index=True)
    data = models.JSONField(default=dict)

    position = models.PointField(blank=True, null=True)
    relevant_images = models.JSONField(default=list, blank=True)

    first_revision = models.DateTimeField(blank=True, null=True, db_index=True)
    approved_by = models.CharField(max_length=200, blank=True, default="")

    municipality = models.ForeignKey(
        Municipality, models.SET_NULL, null=True, blank=True, related_name="monuments"
    )
    province = models.ForeignKey(
        Province, models.SET_NULL, blank=True, null=True, related_name="monuments"
    )
    region = models.ForeignKey(
        Region,
        models.SET_NULL,
        blank=True,
        null=True,
        related_name="monuments",
    )

    app_category_cat = models.ForeignKey(AppCategory, models.CASCADE, blank=True, null=True, related_name="cat_monuments")
    app_category_data = models.ForeignKey(AppCategory, models.CASCADE, blank=True, null=True, related_name="data_monuments")
    app_category = models.ForeignKey(AppCategory, models.CASCADE, blank=True, null=True)
    
    data_categories = models.ManyToManyField(DataCategory, blank=True)

    snapshot = models.ForeignKey(
        Snapshot, models.SET_NULL, null=True, blank=True, related_name="monuments"
    )
    update_snapshot = models.ForeignKey(
        Snapshot,
        models.SET_NULL,
        null=True,
        blank=True,
        related_name="update_monuments",
    )

    first_image_date = models.DateField(blank=True, null=True, db_index=True)
    first_image_date_commons = models.DateField(blank=True, null=True, db_index=True)

    most_recent_wlm_image_date = models.DateField(blank=True, null=True)
    most_recent_commons_image_date = models.DateField(blank=True, null=True)

    current_wlm_state = models.CharField(blank=True, default="", max_length=20)
    current_commons_state = models.CharField(blank=True, default="", max_length=20)

    pictures_count = models.IntegerField(blank=True, default=0)
    pictures_wlm_count = models.IntegerField(blank=True, default=0)
    pictures_commons_count = models.IntegerField(blank=True, default=0)
    to_review = models.BooleanField(default=False)

    in_contest = models.BooleanField(default=False)

    article = models.CharField(max_length=500, blank=True, default="")
    location = models.CharField(max_length=500, blank=True, default="")
    address = models.CharField(max_length=500, blank=True, default="")
    admin_entity = models.CharField(max_length=100, blank=True, default="")

    # TODO: handle this. this is used to understand if a monument can be deleted once queries are run
    # (if all originary queries have been run and the monument is not in any of them)
    sparql_queries = models.ManyToManyField(Query, blank=True)

    disabled = models.BooleanField(default=False)


    class Meta:
        index_together = [
            ("start", "first_revision"),
            ("start", "first_revision", "first_image_date"),
        ]
        ordering = ["label"]

    def __str__(self):
        return self.label


class Picture(models.Model):
    monument = models.ForeignKey(Monument, models.CASCADE, related_name="pictures")
    image_id = models.CharField(max_length=200, unique=True)
    image_url = models.URLField(max_length=2000)
    image_date = models.DateTimeField(blank=True, null=True, db_index=True)
    image_title = models.TextField(blank=True, default="")
    image_type = models.CharField(max_length=20, db_index=True)
    data = models.JSONField(default=dict)
    is_relevant = models.BooleanField(default=False)

    class Meta:
        index_together = [
            ("monument", "image_date"),
        ]


class Contest(models.Model):
    geo_context = models.ForeignKey(GeoContext, models.CASCADE, related_name="contests")
    label = models.CharField(max_length=200)
    label_en = models.CharField(max_length=200, blank=True, default="")
    start_date = models.DateField()
    end_date = models.DateField()
    description = models.TextField(blank=True, default="")
    link = models.URLField(max_length=2000, blank=True, default="")

    def __str__(self):
        return self.label

    @classmethod
    def get_active(cls, tz_name=None):
        if not tz_name:
            now_date = timezone.now().date()
        else:
            tz = pytz.timezone(tz_name)
            now_date = datetime.now(tz).date()

        return Contest.objects.filter(start_date__lte=now_date, end_date__gte=now_date)


class ContestCategory(models.Model):
    contest = models.ForeignKey(
        Contest, models.CASCADE, related_name="contest_categories"
    )
    #the label template
    label_template = models.CharField(max_length=300)


class LocalContest(models.Model):
    contest = models.ForeignKey(
        Contest, models.CASCADE, related_name="local_contests"
    )
    label = models.CharField(max_length=200)
    has_award = models.BooleanField(default=True)
    sparql = models.TextField(blank=True, default="")
    results = models.JSONField(default=dict, editable=False)
    
    provinces = models.ManyToManyField(Province, blank=True)
    regions = models.ManyToManyField(Region, blank=True)
    municipalities = models.ManyToManyField(Municipality, blank=True)
    

    def __str__(self):
        return self.label


class LocalContestException(models.Model):
    local_contest = models.ForeignKey(
        LocalContest, models.CASCADE, related_name="exceptions"
    )
    q_number = models.CharField(max_length=200)
    label = models.TextField(blank=True, default='', editable=False)



# Translations models
class TranslationToken(models.Model):
    """
    Token for translations in frontend (will be exposed and available for translation)
    """
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=300, unique=True)
    
    def __str__(self):
        return self.code



class Language(models.Model):
    """
    Language model
    """
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.code


class GeoContextIntroText(models.Model):
    geo_context = models.ForeignKey(GeoContext, models.CASCADE, related_name="intro_texts")
    language = models.ForeignKey(Language, models.CASCADE, related_name="intro_texts")
    text = models.TextField(blank=True, default="")

    def intro_text_html(self):
        try:
            return markdown.markdown(self.text)    
        except Exception as e:
            return self.text

class GeoContextDonationText(models.Model):
    geo_context = models.ForeignKey(GeoContext, models.CASCADE, related_name="donation_texts")
    language = models.ForeignKey(Language, models.CASCADE, related_name="donation_texts")
    text = models.TextField(blank=True, default="")

    def donation_text_html(self):
        try:
            return markdown.markdown(self.text)    
        except Exception as e:
            return self.text



class Translation(models.Model):
    language = models.ForeignKey(Language, models.CASCADE, related_name="translations")
    
    #optional references to other models useful for translations in django admin via inlines
    app_category = models.ForeignKey(AppCategory, models.CASCADE, blank=True, null=True, related_name="translations")
    translation_token = models.ForeignKey(TranslationToken, models.CASCADE, blank=True, null=True, related_name="translations")

    value = models.TextField()


    def __str__(self):
        return f"{self.language} - {self.translation_token or self.app_category}"


    class Meta:
        
        unique_together = [
            ["language", "app_category", "translation_token"],
        ]



class SimpleLog(models.Model):
    message = models.TextField()
    traceback=models.TextField(blank=True,default="")
    data = models.JSONField(default=dict)
    created = models.DateTimeField(auto_now_add=True)
    