from django.db.models.signals import pre_save, post_save, post_delete
from django.contrib.auth.models import Group, Permission
from django.core.cache import cache
from django.dispatch import receiver
from .models import (
    LocalContestException,
    Monument,
    Language,
    TranslationToken,
    Translation,
    AppCategory,
    GeoContext,
    GeoContextAdmin,
    GeoContextIntroText,
    GeoContextDonationText,
)
from wlm_app_api.helpers import get_wikidata_label, get_other_category_pk


@receiver(
    pre_save,
    sender=LocalContestException,
    dispatch_uid="handle_local_contest_exception_pre_save",
)
def handle_local_contest_exception_pre_save(sender, instance, **kwargs):
    try:
        instance.label = get_wikidata_label(instance.q_number)
    except Exception as e:
        pass


@receiver(pre_save, sender=Monument, dispatch_uid="handle_monument_pre_save")
def handle_monument_pre_save(sender, instance, **kwargs):

    other_category_pk = get_other_category_pk(instance.geo_context_id)

    if (
        instance.app_category_cat_id
        and instance.app_category_cat_id != other_category_pk
    ):
        instance.app_category_id = instance.app_category_cat_id
        return

    elif (
        instance.app_category_data_id
        and instance.app_category_data_id != other_category_pk
    ):
        instance.app_category = instance.app_category_data
        return

    if not instance.app_category_id and other_category_pk:
        instance.app_category_id = other_category_pk


@receiver(
    post_save,
    sender=TranslationToken,
    dispatch_uid="handle_translation_token_post_save",
)
def handle_translation_token_post_save(sender, instance, **kwargs):
    # create translations for all languages
    for language in Language.objects.all():
        Translation.objects.get_or_create(
            translation_token=instance,
            language=language,
            defaults={"value": instance.code},
        )


@receiver(post_save, sender=AppCategory, dispatch_uid="handle_app_category_post_save")
def handle_app_category_post_save(sender, instance, **kwargs):
    # create translations for all languages
    for language in Language.objects.all():
        Translation.objects.get_or_create(
            app_category=instance, language=language, defaults={"value": instance.name}
        )

    # break the cache for get_other_category_pk
    cache_key = f"other_category_{instance.geo_context_id}"
    cache.delete(cache_key)


@receiver(post_save, sender=Language, dispatch_uid="handle_language_post_save")
def handle_language_post_save(sender, instance, **kwargs):
    # create translations for all languages
    for translation_token in TranslationToken.objects.all():
        Translation.objects.get_or_create(
            translation_token=translation_token,
            language=instance,
            defaults={"value": translation_token.code},
        )
    for app_category in AppCategory.objects.all():
        Translation.objects.get_or_create(
            app_category=app_category,
            language=instance,
            defaults={"value": app_category.name},
        )

    for geo_context in GeoContext.objects.all():
        GeoContextIntroText.objects.get_or_create(
            geo_context=geo_context, language=instance
        )
        GeoContextDonationText.objects.get_or_create(
            geo_context=geo_context, language=instance
        )


@receiver(post_delete, sender=Language, dispatch_uid="handle_language_post_delete")
def handle_language_post_delete(sender, instance, **kwargs):
    # Delete all translations associated with the deleted language
    Translation.objects.filter(language=instance).delete()
    # Delete all GeoContextIntroText associated with the deleted language
    GeoContextIntroText.objects.filter(language=instance).delete()
    # Delete all GeoContextDonationText associated with the deleted language
    GeoContextDonationText.objects.filter(language=instance).delete()




@receiver(post_save, sender=GeoContextAdmin, dispatch_uid="handle_geocontext_admin_staff_and_groups")
def handle_geocontext_admin_staff_and_groups(sender, instance, **kwargs):
    # add user to staff 
    user = instance.user
    if not user.is_staff:
        user.is_staff = True
        user.save()

    # ensure existence of a group "management"
    group, created = Group.objects.get_or_create(name='management')
    # if created, add permissions to the group
    models_for_permissions = [
        GeoContext, AppCategory
    ]
    if created or True:
        for model in models_for_permissions:
            for permission in Permission.objects.filter(content_type__app_label=model._meta.app_label):
                print(permission)
                group.permissions.add(permission)


    # add user to group "management"
    user.groups.add(group)
    

    @receiver(post_delete, sender=GeoContextAdmin, dispatch_uid="handle_geocontextadmin_post_delete")
    def handle_geocontextadmin_post_delete(sender, instance, **kwargs):
        # remove user from group "management"
        user = instance.user

        #if there are other geomodeladmin istances attached to the user skip group removal
        should_clear_groups = True
        if GeoContextAdmin.objects.filter(user=user).count() > 0:
            should_clear_groups = False 

        if should_clear_groups:
            try:
                group = Group.objects.get(name='management')
                user.groups.remove(group)
                # remove user from staff
                user.is_staff = False
                user.save()
            except Group.DoesNotExist as e:
                pass