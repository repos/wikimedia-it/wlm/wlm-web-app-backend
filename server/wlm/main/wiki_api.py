import re
import os
from pathlib import Path
import requests
import time
import csv
import urllib.parse
from retry import retry

CURRENT_DIR = Path(__file__).resolve().parent
SPARQL_URL = "https://query.wikidata.org/sparql"
COMMONS_URL = "https://commons.wikimedia.org/w/api.php"
API_URL = "https://www.wikidata.org/w/api.php"


def load_wiki_candidate_types():
    file_path = CURRENT_DIR / "WIKI_CANDIDATE_TYPES.csv"
    with open(file_path, "r") as f:
        reader = csv.DictReader(f, delimiter=",")
        #allowing comments...
        return [x for x in list(reader) if not x.get("q_number").startswith("#")]


WIKI_CANDIDATE_TYPES = load_wiki_candidate_types()

WLM_QUERIES = [
    {"label": "in contest", "query_file" : "SPARQL-contest.txt", "q_number": "W0", "group": "Contest"},
    {"label": "municipality overview picture", "query_file" : "SPARQL-municipalities-views.txt", "q_number": "W1", "group": "Contest"},
    {"label": "fortificazioni (IIC 2022)", "query_file" : "SPARQL-fortifications.txt", "q_number": "W2", "group": "Contest"},
    {"label": "religious building", "query_file" : "SPARQL-religious-buildings.txt", "q_number": "W3", "group": "Contest"},
]

def get_wlm_query(query_file):
    SPARQL_CONTEST_PATH = CURRENT_DIR / query_file
    with open(SPARQL_CONTEST_PATH, "r") as f:
        QUERY_TEMPLATE = f.read()
        f.close()
    return QUERY_TEMPLATE


def get_query_template_typologies():
    SPARQL_TYPOLOGIES_PATH = CURRENT_DIR / "SPARQL-typologies.txt"
    with open(SPARQL_TYPOLOGIES_PATH, "r") as f:
        QUERY_TEMPLATE = f.read()
        f.close()
    return QUERY_TEMPLATE


def execute_query(query, limit=None, offset=None, q_number_region=None):
    if limit:
        query = query + f" LIMIT {limit}"
    if offset:
        query = query + f" OFFSET {offset}"

    if q_number_region:
        query = query.replace("Q_NUMBER_REGION", q_number_region)

    headers = {'User-Agent': 'WikiLovesMonumentsApp/0.0 (https://app.wikilovesmonuments.it; contatti@wikilovesmonuments.it)'}

    r = requests.get(SPARQL_URL, params={"format": "json", "query": query}, timeout=400, headers=headers)
    
    try:
        return r.json()
    except Exception as e:
        raise Exception(r.text)


def format_monument(monument):
    out = {}
    for key in monument:
        value = monument[key]["value"]
        # clean q_numbers
        if "http://www.wikidata.org/entity/" in value:
            value = value.replace("http://www.wikidata.org/entity/", "")
        if "http://www.wikidata.org/entity/statement/" in value:
            value = value.replace("http://www.wikidata.org/entity/statement/", "")
            value = value.split("-")[0]
                
            
        # split lists into arrays
        if key.endswith("_n"):
            if value == "":
                value = []
            else:
                value = value.split(";")
        out[key] = value
    return out


def search_commons_url(url):    

    filename = url.split("FilePath/")[-1]
    payload = {
        "action": "query",
        "format": "json",
        "prop": "imageinfo",
        "generator": "search",
        "iiprop": "extmetadata",
        "gsrsearch": f"prefix:file:{filename}",
        "gsroffset": "0",
        
    }

    next_page = True
    out = []

    while next_page:
        params = "&".join("%s=%s" % (k,v) for k,v in payload.items())
        r = requests.get(COMMONS_URL, params)
        data = r.json()
        
        if "query" in data and "pages" in data["query"] and len(data["query"]["pages"]) > 0:
            for pageid in data["query"]["pages"]:
                image = data["query"]["pages"][str(pageid)]
                temp_obj = {}
                if "pageid" in image:
                    temp_obj["pageid"] = image["pageid"]
                if "title" in image:
                    temp_obj["title"] = image["title"]
                if "imageinfo" in image and len(image["imageinfo"]) > 0 and "extmetadata" in image["imageinfo"][0]:
                    extmetadata = image["imageinfo"][0]["extmetadata"]
                    if "DateTimeOriginal" in extmetadata and "value" in extmetadata["DateTimeOriginal"]:
                        temp_obj["DateTimeOriginal"] = extmetadata["DateTimeOriginal"]["value"]
                    if "DateTime" in extmetadata and "value" in extmetadata["DateTime"]:
                        temp_obj["DateTime"] = extmetadata["DateTime"]["value"]

                    if "Artist" in extmetadata and "value" in extmetadata["Artist"]:
                        temp_obj["Artist"] = extmetadata["Artist"]["value"]
                    if "License" in extmetadata and "value" in extmetadata["License"]:
                        temp_obj["License"] = extmetadata["License"]["value"]
                    if "ImageDescription" in extmetadata and "value" in extmetadata["ImageDescription"]:
                        temp_obj["ImageDescription"] = extmetadata["ImageDescription"]["value"]

                out.append(temp_obj)
        else:
            next_page = False
        
        if "continue" in data and "gsroffset" in data["continue"]:
            params["gsroffset"] = data["continue"]["gsroffset"]
        else:
            next_page = False
    return out 

@retry(tries=5, delay=45)
def search_commons_wlm(q_number, wlm_id):
    params = {
        "action": "query",
        "format": "json",
        "prop": "imageinfo",
        "generator": "search",
        "iiprop": "extmetadata",
        "gsrsearch": '"' + wlm_id + '" "Wiki Loves Monuments Italia"',
        "gsrnamespace": "6",
        "gsrlimit": "500",
        "grsoffset": "0",
    }
    
    next_page = True
    out = []
    read = 0
    while(next_page):
        r = requests.get(COMMONS_URL, params)
        data = r.json()

        if "query" in data and "pages" in data["query"] and len(data["query"]["pages"]) > 0:
            for pageid in data["query"]["pages"]:
                image = data["query"]["pages"][str(pageid)]
                read += 1
            
                temp_obj = {}
                
                if "pageid" in image:
                    temp_obj["pageid"] = str(image["pageid"]) + q_number
                if "title" in image:
                    temp_obj["title"] = image["title"]
                if "imageinfo" in image and len(image["imageinfo"]) > 0 and "extmetadata" in image["imageinfo"][0]:
                    extmetadata = image["imageinfo"][0]["extmetadata"]
                    if "DateTimeOriginal" in extmetadata and "value" in extmetadata["DateTimeOriginal"]:
                        temp_obj["DateTimeOriginal"] = extmetadata["DateTimeOriginal"]["value"]
                    if "DateTime" in extmetadata and "value" in extmetadata["DateTime"]:
                        temp_obj["DateTime"] = extmetadata["DateTime"]["value"]

                    if "Artist" in extmetadata and "value" in extmetadata["Artist"]:
                        temp_obj["Artist"] = extmetadata["Artist"]["value"]
                    if "License" in extmetadata and "value" in extmetadata["License"]:
                        temp_obj["License"] = extmetadata["License"]["value"]
                    if "ImageDescription" in extmetadata and "value" in extmetadata["ImageDescription"]:
                        temp_obj["ImageDescription"] = extmetadata["ImageDescription"]["value"]

                out.append(temp_obj)
        else:
            next_page = False
            
        
        if "continue" in data and  "grsoffset" in data["continue"]:
            params.update(data["continue"])
            time.sleep(1)
        else:
            next_page = False
    
    return out


@retry(tries=5, delay=45)
def search_commons_cat(q_number, cat):    

    payload = {
        "action": "query",
        "format": "json",
        "generator": "categorymembers",
        "prop": "imageinfo",
        "iiprop": "extmetadata",
        "gcmtype": "file",
        "gcmtitle": "Category:" + cat,
        "gcmnamespace" : "0|6|12|14|100|106",
    }

    next_page = True
    out = []
    read = 0
    while next_page:
        params = "&".join("%s=%s" % (k,v) for k,v in payload.items())
        r = requests.get(COMMONS_URL, params)
        data = r.json()
        
        if "query" in data and "pages" in data["query"] and len(data["query"]["pages"]) > 0:
            
            for pageid in data["query"]["pages"]:
                image = data["query"]["pages"][str(pageid)]
                read += 1
                
                temp_obj = {}
                if "pageid" in image:
                    temp_obj["pageid"] = str(image["pageid"]) + q_number
                    
                if "title" in image:
                    temp_obj["title"] = image["title"]
            

                if "imageinfo" in image and len(image["imageinfo"]) > 0 and "extmetadata" in image["imageinfo"][0]:
                    extmetadata = image["imageinfo"][0]["extmetadata"]
                    if "DateTimeOriginal" in extmetadata and "value" in extmetadata["DateTimeOriginal"]:
                        temp_obj["DateTimeOriginal"] = extmetadata["DateTimeOriginal"]["value"]
                    if "DateTime" in extmetadata and "value" in extmetadata["DateTime"]:
                        temp_obj["DateTime"] = extmetadata["DateTime"]["value"]

                    if "Artist" in extmetadata and "value" in extmetadata["Artist"]:
                        temp_obj["Artist"] = extmetadata["Artist"]["value"]
                    if "License" in extmetadata and "value" in extmetadata["License"]:
                        temp_obj["License"] = extmetadata["License"]["value"]
                    if "ImageDescription" in extmetadata and "value" in extmetadata["ImageDescription"]:
                        temp_obj["ImageDescription"] = extmetadata["ImageDescription"]["value"]

                out.append(temp_obj)
        else:
            next_page = False
            
        if "continue" in data and "batchcomplete" in data:
            payload.update(data["continue"])
            time.sleep(1)
        else:
            next_page = False
    return out 

def get_revision(q):
    
    params = {
        "action": "query",
        "format": "json",
        "prop": "revisions",
        "titles": q,
        "rvprop": "timestamp",
        "rvlimit": "1",
        "rvdir":"newer"
      }
    r = requests.get(API_URL, params)
    results = r.json()
    pages = results["query"]["pages"]
    keys = list(pages.keys())
    if not len(keys):
        return None
    
    return pages[keys[0]]["revisions"][0]["timestamp"]