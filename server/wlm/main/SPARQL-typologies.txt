SELECT DISTINCT
  ?mon ?monLabel
  ?locationLabel
  ?article
  
  (group_concat(DISTINCT ?parent; separator=";") as ?parent_n)
  (group_concat(DISTINCT ?geo; separator=";") as ?geo_n)
  (group_concat(DISTINCT ?place; separator=";") as ?place_n)
  (group_concat(DISTINCT ?place_geo; separator=";") as ?place_geo_n)
  (group_concat(DISTINCT ?wlm; separator=";") as ?wlm_n)
  (group_concat(DISTINCT ?start; separator=";") as ?start_n)
  (group_concat(DISTINCT ?end; separator=";") as ?end_n)
  (group_concat(DISTINCT ?approvedBy; separator=";") as ?approvedBy_n)
  (group_concat(DISTINCT ?relevantImage; separator=";") as ?relevantImage_n)
  (group_concat(DISTINCT ?commonsCat; separator=";") as ?commons_n)
  (SAMPLE(?address) as ?address)
  (SAMPLE(?place) as ?adminEntity)
  (SAMPLE(?location) as ?location)

WHERE {
  VALUES ?tipi {
    wd:Q_NUMBER_TYPE
  }
  
  SERVICE wikibase:label { bd:serviceParam wikibase:language "it,en,[AUTO_LANGUAGE]". }
  
      ?mon wdt:P17 wd:Q38.
      FILTER EXISTS { ?mon      wdt:P31/wdt:P279* ?tipi . }
      #selezione regione parametrizzata a run time
      ?mon      wdt:P131* wd:Q_NUMBER_REGION.
  
  MINUS {
    ?mon wdt:P31 ?tipo .
    FILTER ( ?tipo = wd:Q131647 || ?tipo = wd:Q973011 || ?tipo = wd:Q4618 || ?tipo = wd:Q288514 )
  }
  
  OPTIONAL { ?mon p:P2186 ?wlms . 
    OPTIONAL { ?wlms ps:P2186 ?wlm . }
    OPTIONAL { ?wlms pq:P580 ?start . }
    OPTIONAL { ?wlms pq:P582 ?end . }
    OPTIONAL { ?wlms pq:P790 ?approvedBy. }
  }
  OPTIONAL { ?mon wdt:P361 ?parent }
  OPTIONAL { ?mon wdt:P625 ?geo. }
  OPTIONAL {
    ?mon wdt:P131 ?place .
    OPTIONAL {
      ?place wdt:P625 ?place_geo .
    }
  }
  OPTIONAL { ?mon wdt:P18 ?relevantImage . }
  OPTIONAL { ?mon wdt:P373 ?commonsCat . }
  OPTIONAL { ?mon wdt:P276 ?location . }
  OPTIONAL {
    ?article schema:about ?mon ;
    schema:isPartOf <https://it.wikipedia.org/> .
  }
  OPTIONAL { ?mon wdt:P6375 ?address . }
}

GROUP BY ?mon ?monLabel ?locationLabel ?article
