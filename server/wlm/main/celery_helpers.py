from celery import current_app as celery_app
from celery.result import AsyncResult
from main.tasks import snap_context, snap_commons
from main.models import GeoContext
import logging

logger = logging.getLogger(__name__)

def kill_traced_tasks(trace_id):
    i = celery_app.control.inspect()
    
    active =  i.active()
    if not active:
        return
    
    workers = active.values()

    try:
        main_task = AsyncResult(trace_id)
        main_task.abort()
    except Exception as e:
        celery_app.control.revoke(trace_id, terminate=True, signal="SIGKILL")
    
    for worker in workers:
        for task in worker:
            logger.info(f'{task["name"]}, {task["id"]}, {task["kwargs"]}')
            kwargs = task["kwargs"]
            if "trace_id" in kwargs:
                if kwargs["trace_id"] == trace_id:
                    logger.info(f'Terminating task {task["id"]}')
                    try:
                        celery_app.control.revoke(task["id"], terminate=True, signal="SIGKILL")
                    except Exception as e:
                        pass
                    
                    try:
                        res = AsyncResult(task["id"])
                        res.abort()
                    except Exception as e:
                        pass
                    



def start_snapshot_for_context(geo_context_id, continue_snapshot_id=None, perform_commons=True):
    geo_context = GeoContext.objects.get(pk=geo_context_id)
    # look for pending snapshots
    pending_snapshots = geo_context.snapshots.filter(complete=False, killed=False)
    if pending_snapshots.exists():
        return False
    
    snap_context.delay(geo_context_id, continue_snapshot_id=continue_snapshot_id, perform_commons=perform_commons)
    return True


def start_snapshot_cron(geo_context_id=None, perform_commons=True):
    if geo_context_id is None:
        raise ValueError("geo_context_id is required")
    
    return snap_context.delay(geo_context_id, perform_commons=perform_commons)



def snap_commons_cron(geo_context_id=None):
    if geo_context_id is None:
        raise ValueError("geo_context_id is required")
    
    return snap_commons.delay(geo_context_id)
    



def get_queued_snapshot(geo_context_id):
    """
    scans celery for queued with the given geo_context_id as a keyword argument
    """
    i = celery_app.control.inspect()
    active =  i.active()
    if not active:
        return None
    
    workers = active.values()
    for worker in workers:
        for task in worker:
            kwargs = task["kwargs"]
            if task["name"] == "main.tasks.snap_context" and "geo_context_id" in kwargs:
                if kwargs["geo_context_id"] == geo_context_id:
                    return task["id"]
    return None