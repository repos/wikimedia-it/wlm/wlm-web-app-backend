from django.apps import AppConfig


class MainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main'
    signals_imported = False

    def ready(self):
        if not self.signals_imported:
            import main.signals
            self.signals_imported = True
        
