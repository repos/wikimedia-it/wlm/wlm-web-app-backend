import json
import requests
#from sentry_sdk import capture_exception
from itertools import product, chain
from celery import shared_task, group, chord, chain as celery_chain
from celery.result import allow_join_result
from celery.contrib.abortable import AbortableTask
from main.models import (
    GeoContext,
    Snapshot,
    Query,
    QuerySnapshot,
    AppCategory,
    WLMCategoryRule,
    Monument,
    SimpleLog,
)
from main.helpers import update_monument, update_monument_pictures
from main.wiki_api import format_monument
from main.category_helpers import categorize_monument
import time
from retry import retry
from django.db import models, transaction
from django.conf import settings



SPARQL_URL = "https://query.wikidata.org/sparql"
import logging

logger = logging.getLogger(__name__)


# todo: this is duplicated in wiki_api.py


@retry(tries=8, delay=20, backoff=1.2)
def execute_query(query, limit=None, offset=None):
    if limit:
        query = query + f" LIMIT {limit}"
    if offset:
        query = query + f" OFFSET {offset}"

    headers = {
        "User-Agent": "WikiLovesMonumentsApp/0.0 (https://app.wikilovesmonuments.it; contatti@wikilovesmonuments.it)"
    }
    r = requests.get(
        SPARQL_URL,
        params={"format": "json", "query": query},
        timeout=400,
        headers=headers,
    )

    try:
        return r.json()
    except Exception as e:
        raise Exception(r.text)


@shared_task(rate_limit="16/m")
def execute_query_chunks(query, chunk_size=100, trace_id=None):
    more_results = True
    offset = 0
    results = []
    while more_results:
        r = execute_query(query, limit=chunk_size, offset=offset)
        if len(r["results"]["bindings"]) < chunk_size:
            more_results = False
        results.extend(r["results"]["bindings"])
        offset += chunk_size

    return results


@shared_task
def print_query_results(results_list):
    data = list(chain.from_iterable(results_list))
    print("got results!", len(data))


@shared_task
def complete_query_snapshot(*results, query_snapshot_id=None):
    query_snapshot = QuerySnapshot.objects.get(id=query_snapshot_id)
    query_snapshot.complete = True
    query_snapshot.task_status = "success"
    query_snapshot.save()

    # check if snapshot is complete
    snapshot = query_snapshot.snapshot
    if snapshot.query_snapshots.filter(complete=False).count() == 0:
        snapshot.complete = True
        snapshot.task_status = "success"
        snapshot.save()

    return query_snapshot.pk


@shared_task(bind=True, base=AbortableTask, rate_limit="18/m", queue="update_monuments")
def update_monuments_data(self, mon_list, snapshot_id, trace_id=None):
    snap = Snapshot.objects.get(id=snapshot_id)

    done_items = 0

    for item in mon_list:
        try:
            update_monument(item, snap, skip_pictures=True)
            done_items += 1
            if done_items % 100 == 0:
                logger.info(f"Updated {done_items} monuments")
                snaps = Snapshot.objects.select_for_update().filter(pk=snapshot_id)
                with transaction.atomic():
                    for snap in snaps:
                        snap.done_monuments += done_items
                        done_items = 0
                        snap.save()
                
        except Exception as e:
            logger.error(f"Error updating monument {item}")
        
        if self.is_aborted():
            # respect aborted state, and terminate gracefully.
            logger.warning("Task aborted")
            return
    
    if done_items > 0:
        logger.info(f"Updated {done_items} monuments")
        snaps = Snapshot.objects.select_for_update().filter(pk=snapshot_id)
        with transaction.atomic():
            for snap in snaps:
                snap.done_monuments += done_items
                snap.save()


@shared_task
def process_all_queries(snapshot_id, trace_id=None):
    snapshot = Snapshot.objects.get(id=snapshot_id)
    
    query_snapshots = snapshot.query_snapshots.filter(complete=True)
    query_snapshot = query_snapshots.annotate(
        weight=models.Case(
            models.When(query__categorize_for_app=True, then=1),
            default=0,
        )
    ).order_by("-weight")
    all_monuments = {}

    try:
        app_category_other = AppCategory.objects.get(
            is_other_monuments=True, geo_context=snapshot.geo_context
        )
    except AppCategory.DoesNotExist:
        app_category_other = None

    rules = WLMCategoryRule.objects.filter(geo_context=snapshot.geo_context).order_by(
        "order"
    )

    for query_snapshot in query_snapshots:
        data_categories = query_snapshot.query.data_categories.all()

        for mon in query_snapshot.payload:
            item = categorize_monument(
                format_monument(mon),
                data_categories,
                query_snapshot.query.categorize_for_app,
                app_category_other=app_category_other,
                rules=rules,
            )

            if not item.get("mon", None):
                continue

            if item["mon"] not in all_monuments:
                all_monuments[item["mon"]] = item
            
            else:
            
                if "app_category_cat" in item:
                    
                    if "app_category_cat" not in all_monuments[item["mon"]]:
                        all_monuments[item["mon"]]["app_category_cat"] = item["app_category_cat"] 
                    
                    elif (
                        app_category_other
                        and all_monuments[item["mon"]]["app_category_cat"] == app_category_other.pk
                    ):
                        all_monuments[item["mon"]]["app_category_cat"] = item["app_category_cat"]

                if "app_category_data" in item:

                    if "app_category_data" not in all_monuments[item["mon"]]:
                        all_monuments[item["mon"]]["app_category_data"] = item["app_category_data"] 
                    
                    elif (
                        app_category_other
                        and all_monuments[item["mon"]]["app_category_data"] == app_category_other.pk
                    ):
                        all_monuments[item["mon"]]["app_category_data"] = item["app_category_data"]

                # TODO: handle "data_categories" field

    
    
    
    
    monuments = list(all_monuments.values())

    ALL_VALUES_PATH = settings.BASE_DIR / "all_values.json"
    with open(ALL_VALUES_PATH, "w") as f:
        f.write(json.dumps(monuments))

    num_monuments = len(monuments)
    snapshot.total_monuments = num_monuments
    snapshot.save()

    chunk_size = 5000
    signatures = []
    for i in range(0, len(monuments), chunk_size):
        chunk = monuments[i : i + chunk_size]
        signatures.append(
            update_monuments_data.s(chunk, snapshot_id, trace_id=trace_id)
        )

    res = group(signatures)()
    res.get()

    # snapshot.complete = True
    # snapshot.task_status = "success"
    # snapshot.save()


@shared_task(bind=True, base=AbortableTask, rate_limit="18/m", queue="update_commons")
def update_monuments_commons(self, snapshot_id, mon_list, trace_id=None):
    for idx, pk in enumerate(mon_list):
        if idx % 20 == 0:
            snapshot = Snapshot.objects.get(id=snapshot_id)
            if snapshot.killed:
                logger.warning("Task killed")
                return
            
        if self.is_aborted() :
            # respect aborted state, and terminate gracefully.
            logger.warning("Task aborted")
            return
        try:
            update_monument_pictures(pk, reset_pictures=True)
        except:
            logger.error(f"Error updating commons for monument {pk}")
        

@shared_task(bind=True, base=AbortableTask)
def process_commons(self, snapshot_id, trace_id=None):
    snapshot = Snapshot.objects.get(id=snapshot_id)
    monuments = snapshot.monuments.all().values_list("pk", flat=True)
    chunk_size = 1000
    signatures = []
    
    for i in range(0, len(monuments), chunk_size):
        if self.is_aborted():
            # respect aborted state, and terminate gracefully.
            logger.warning("Task aborted")
            return
        chunk = monuments[i : i + chunk_size]
        signatures.append(update_monuments_commons.s(snapshot_id, chunk, trace_id=trace_id))

    c = group(signatures)
    result = c()
    result.get()


@shared_task(rate_limit="10/m", queue="execute_sparql")
def execute_sparql_query(
    snapshot_id, query_id, concurrency=4, chunk_size=20000, trace_id=None
):
    """
    executor for handling placeholders in sparql queries
    """

    logger.info("executing sparql query")

    query = Query.objects.get(id=query_id)

    query_snapshot, created = QuerySnapshot.objects.get_or_create(
        snapshot_id=snapshot_id, query=query, defaults={"complete": False}
    )

    if created or not query_snapshot.complete:

        # sparql data fetching
        placeholders = []
        placeholders_values = []
        if query.placeholder:
            placeholders = [query.placeholder.symbol]
            placeholders_values = [
                query.placeholder.values.all().values_list("value", flat=True)
                #['Q1210']
            ]

        sparql_query = query.sparql

        # generate the parallel queries
        generated_queries = []
        placeholders_tuples = list(product(*placeholders_values))

        next_query = sparql_query
        for placeholders_tuple in placeholders_tuples:
            for i, placeholder in enumerate(placeholders):
                next_query = sparql_query.replace(placeholder, placeholders_tuple[i])
            generated_queries.append(next_query)

        signatures = []
        for g_query in generated_queries:
            signatures.append(
                execute_query_chunks.s(
                    g_query, chunk_size=chunk_size, trace_id=trace_id
                )
            )

        query_snapshot.task_status = "running"
        query_snapshot.save()

        c = group(signatures)()
        results_list = c.get()
        payload = list(chain.from_iterable(results_list))
        query_snapshot.payload = payload
        query_snapshot.total_monuments = len(query_snapshot.payload)
        query_snapshot.complete = True
        query_snapshot.save()

        return query_snapshot.id

    else:
        query_snapshot.total_monuments = len(query_snapshot.payload)
        query_snapshot.save()
        return query_snapshot.id


@shared_task
def snapshot_error_handler(request, exc, traceback):
    # print('Task {0} raised exception: {1!r}\n{2!r}'.format(
    #       request.id, exc, traceback))
    print(dir(request))
    print(request.properties)
    snapshot_id = request.properties["snap_id"]
    snap = Snapshot.objects.get(id=snapshot_id)
    snap.task_status = "error"
    snap.task_error = traceback
    snap.complete = True
    snap.save()
    


@shared_task
def query_snapshot_error_handler(request, exc, traceback):
    print("Task {0} raised exception: {1!r}\n{2!r}".format(request.id, exc, traceback))
    print(dir(request))
    SimpleLog.objects.create(
        message=str(exc),
        traceback=str(traceback),
        data={
            "snap_id": request.properties["snap_id"],
        }
    )


@shared_task
def complete_snapshot(res, snapshot_id, status):
    snap = Snapshot.objects.get(id=snapshot_id)
    snap.task_status = status
    snap.complete = True
    snap.save()


@shared_task(bind=True, base=AbortableTask)
def snap_context(self, geo_context_id, continue_snapshot_id=None, perform_commons=True):

    print("executing snap context")

    geo_context = GeoContext.objects.get(pk=geo_context_id)
    # look for pending snapshots
    pending_snapshots = geo_context.snapshots.filter(complete=False, killed=False)
    if pending_snapshots.exists():
        logger.info(f"Found pending snapshots for {geo_context.label}")
        last_snap = pending_snapshots.last()
        logger.info(last_snap)
        return

    if continue_snapshot_id:
        snap = Snapshot.objects.get(pk=continue_snapshot_id)
        snap.complete = False
        snap.killed = False
        snap.task_id = self.request.id
        snap.task_status = "running"
        snap.perform_commons = perform_commons
        snap.save()

    else:
        # create a new snapshot
        snap = Snapshot.objects.create(
            geo_context=geo_context, task_id=self.request.id, task_status="running",
            perform_commons=perform_commons
        )

    snap.refresh_from_db()
    snap.task_status = "running sparql queries"
    snap.save()

    # get the queries related to the geo context
    queries = (
        geo_context.sparql_queries.all()
        .annotate(
            weight=models.Case(
                models.When(categorize_for_app=True, then=1),
                default=0,
            )
        )
        .order_by("-weight")
    )

    # map approach
    for query in queries:
        if not self.is_aborted():
            res = execute_sparql_query.s(
                snap.id, query.id, trace_id=self.request.id
            ).apply_async(link_error=query_snapshot_error_handler.s())
            try:
                id = res.get()
            except Exception as e:
                snap.task_status = "error"
                snap.task_error = str(e)
                snap.killed = True
                snap.save()
                return self.request.id
            time.sleep(5)

    snap.refresh_from_db()
    snap.task_status = "updating database"
    snap.save()

    process_all_queries(snap.id, trace_id=self.request.id)

    if perform_commons:
        snap.refresh_from_db()
        snap.task_status = "updating commons"
        snap.save()

        c = process_commons.s(snap.id, trace_id=self.request.id).apply_async()
        res = c.get()


    snap.refresh_from_db()
    
    #dropping monuments that are not in the snapshot
    Monument.objects.filter(geo_context=geo_context).exclude(update_snapshot=snap).delete()

    snap.task_status = "complete"
    snap.complete = True

    snap.save()

    return self.request.id


@shared_task(bind=True, base=AbortableTask)
def snap_commons(self, geo_context_id):

    geo_context = GeoContext.objects.get(pk=geo_context_id)

    #ensure that there are no pending snapshots (not complete and not killed) for the current geo_context
    pending = geo_context.snapshots.filter(complete=False, killed=False)
    if pending.exists():
        logger.info(f"Found pending snapshots for {geo_context.label}")
        return

    #find last snapshot for the geo context that is complete
    snapshot = geo_context.snapshots.filter(complete=True).order_by('created').last()

    if not snapshot:
        raise Exception("No complete snapshot to process")
    
    snapshot = Snapshot.objects.get(pk=snapshot.id)
    snapshot.perform_commons = True
    snapshot.complete = False
    snapshot.killed = False
    snapshot.save()

    c = process_commons.s(snapshot.id, trace_id=self.request.id).apply_async()
    res = c.get()

    snapshot.task_status = "complete"
    snapshot.complete = True
    snapshot.save()

    return self.request.id

    