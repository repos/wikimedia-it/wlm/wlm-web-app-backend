# Generated by Django 4.2 on 2024-08-20 13:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0048_alter_translation_unique_together_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='language',
            name='code',
            field=models.CharField(default='oo', max_length=10, unique=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='translationtoken',
            name='code',
            field=models.CharField(default='oo', max_length=300, unique=True),
            preserve_default=False,
        ),
        migrations.RemoveField(
            model_name='language',
            name='id',
        ),
        migrations.RemoveField(
            model_name='translationtoken',
            name='id',
        ),
        # migrations.AddField(
        #     model_name='language',
        #     name='id',
        #     field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        # ),
        # migrations.AddField(
        #     model_name='translationtoken',
        #     name='id',
        #     field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        # ),
    ]
