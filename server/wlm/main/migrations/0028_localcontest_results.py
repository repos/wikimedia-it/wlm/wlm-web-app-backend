# Generated by Django 4.2 on 2024-08-06 15:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0027_localcontest_sparql'),
    ]

    operations = [
        migrations.AddField(
            model_name='localcontest',
            name='results',
            field=models.JSONField(default=dict, editable=False),
        ),
    ]
