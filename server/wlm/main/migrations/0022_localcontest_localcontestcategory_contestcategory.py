# Generated by Django 4.2 on 2024-08-02 21:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0021_wlmcategoryrule_geo_context'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocalContest',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('label', models.CharField(max_length=200)),
                ('contest', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='local_contests', to='main.contest')),
            ],
        ),
        migrations.CreateModel(
            name='LocalContestCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('label_template', models.CharField(max_length=300)),
                ('local_contest', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contest_categories', to='main.localcontest')),
            ],
        ),
        migrations.CreateModel(
            name='ContestCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('label_template', models.CharField(max_length=300)),
                ('contest', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contest_categories', to='main.contest')),
            ],
        ),
    ]
