# Generated by Django 4.2 on 2024-08-05 12:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0023_geocontextadmin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='geocontextadmin',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='admin_geo_contexts', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='wlmcategoryrulepredicate',
            name='property',
            field=models.CharField(choices=[('mon', 'mon'), ('commonsCat', 'commonsCat'), ('wlm', 'wlm'), ('instanceOf_n', 'instanceOf_n (P31)'), ('parent_n', 'parent_n'), ('place_n', 'place_n'), ('approvedBy_n', 'approvedBy_n (P790)'), ('endorsedBy_n', 'endorsedBy_n (P8001)'), ('accreditedBy_n', 'accreditedBy_n (P5514)'), ('adminEntity', 'adminEntity'), ('location', 'location')], max_length=200),
        ),
    ]
