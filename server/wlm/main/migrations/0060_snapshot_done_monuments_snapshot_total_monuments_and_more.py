# Generated by Django 4.2 on 2024-09-15 22:51

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0059_geocontext_donations_popup_probability_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='snapshot',
            name='done_monuments',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='snapshot',
            name='total_monuments',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='geocontext',
            name='donations_popup_probability',
            field=models.IntegerField(blank=True, default=0, help_text='Probability of showing the donations popup (0-100)', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)]),
        ),
        migrations.AlterField(
            model_name='geocontext',
            name='enable_donations_text',
            field=models.BooleanField(default=False, help_text='Enable donations text in the app'),
        ),
    ]
