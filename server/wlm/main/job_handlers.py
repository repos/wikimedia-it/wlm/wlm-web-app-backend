from django.core.management import call_command

def clean_snapshots(num_snapshots=2, **_kwargs):
    call_command("clean_snapshots", "--num-snapshots", num_snapshots)
