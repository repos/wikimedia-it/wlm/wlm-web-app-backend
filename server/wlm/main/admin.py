from django.contrib import messages
from django.contrib.gis import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.auth.models import User
from django.db import transaction
from django.utils.html import format_html, mark_safe
from django.utils.translation import gettext_lazy as _
from django.db import models
from django import forms
from main.models import (
    GeoContext,
    Query,
    QuerySnapshot,
    Region,
    Province,
    Municipality,
    Monument,
    Picture,
    Snapshot,
    AppCategory,
    DataCategory,
    Contest,
    WLMCategoryRulePredicate,
    WLMCategoryRule,
    Icon,
    LocalContest,
    LocalContestException,
    GeoContextAdmin,
    QueryPlaceholder,
    QueryPlaceholderValue,
    Language,
    TranslationToken,
    Translation,
    GeoContextIntroText,
    GeoContextDonationText,
    SimpleLog,
)
from django_ace import AceWidget
import nested_admin


class MonumentAdminEntityFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _("Admin entity <> municipality")

    # Parameter for the filter that will be used in the URL query.
    parameter_name = "admin_entity_to_fix"

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return [
            (True, _("True")),
            (False, _("False")),
        ]

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.

        if self.value() == False:
            return queryset.exclude(admin_entity=models.F("municipality__code"))

        if self.value() == True:
            return queryset.filter(admin_entity=models.F("municipality__code"))

        return queryset


class LimitGeoContextFilter(SimpleListFilter):
    title = _("Geo context")

    # Parameter for the filter that will be used in the URL query.
    parameter_name = "geo_context"

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            return [(g.pk, g.label) for g in GeoContext.objects.all()]
        admins = GeoContextAdmin.objects.filter(user=request.user)
        return [(a.geo_context.pk, a.geo_context.label) for a in admins]

    def queryset(self, request, queryset):
        if self.value():
            geo_context_pk_kwarg = f"{self.parameter_name}__pk"
            filter_kwargs  = {
                geo_context_pk_kwarg : self.value()
            }
            queryset = queryset.filter(**filter_kwargs)

            if request.user.is_superuser:
                return queryset
            admins = GeoContextAdmin.objects.filter(user=request.user)
            geo_context_in_kwarg = f"{self.parameter_name}__in"
            return queryset.filter(**{geo_context_in_kwarg: [a.geo_context for a in admins]})
            
        return queryset
    
    
class LimitGeoContextContestFilter(LimitGeoContextFilter):
    # Parameter for the filter that will be used in the URL query.
    parameter_name = "contest__geo_context"


    
class LimitedGeoContextMixin(object):

    geo_context_field = "geo_context"
    limited_field = "geo_context"
    limited_lookup_field = "pk"

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        admins = GeoContextAdmin.objects.filter(user=request.user)

        geo_context_kwarg = f"{self.geo_context_field}__in"
        kwargs = {
            geo_context_kwarg: [a.geo_context for a in admins]
        }

        return qs.filter(**kwargs)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):

        field = super().formfield_for_foreignkey(db_field, request, **kwargs)
        if request.user.is_superuser:
            return field
        
        if db_field.name == self.limited_field:
            admins = GeoContextAdmin.objects.filter(user=request.user)
            kwargs_lookup = f"{self.limited_lookup_field}__in"
            kwargs = {
                kwargs_lookup: [a.geo_context.pk for a in admins]
            }
            field.queryset = field.queryset.filter(
                **kwargs
            )

        return field


class TranslationForm(forms.ModelForm):
    class Meta:
        model = Translation
        fields = ["value"]


class TranslationInline(admin.TabularInline):
    model = Translation
    extra = 0
    form = TranslationForm
    readonly_fields = ["language"]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class RegionAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    list_filter = [LimitGeoContextFilter]
    search_fields = ["name", "code"]


admin.site.register(Region, RegionAdmin)


class ProvinceAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    search_fields = ["name", "code"]
    list_filter = ["region", LimitGeoContextFilter]


admin.site.register(Province, ProvinceAdmin)


class MunicipalityAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    search_fields = ["name", "code"]
    list_filter = [LimitGeoContextFilter, "region", "province"]


admin.site.register(Municipality, MunicipalityAdmin)


class QueryForm(forms.ModelForm):
    class Meta:
        model = Query
        widgets = {
            "sparql": AceWidget(
                mode="sparql", theme="mono", width="80%", height="400px"
            ),
        }
        fields = "__all__"


class QueryAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    list_filter = [LimitGeoContextFilter]
    form = QueryForm


admin.site.register(Query, QueryAdmin)


class DataCategoryAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    list_filter = [LimitGeoContextFilter]


admin.site.register(DataCategory, DataCategoryAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ["label", "q_number", "group", "app_category"]
    list_filter = [LimitGeoContextFilter, "app_category"]
    search_fields = ["label", "q_number"]


class PictureAdmin(admin.ModelAdmin):
    raw_id_fields = ("monument",)
    list_filter = ["monument__geo_context", "is_relevant"]
    search_fields = ["monument__label", "monument__q_number"]


admin.site.register(Picture, PictureAdmin)


class SnapshotAdmin(admin.ModelAdmin):
    readonly_fields = ["updated", "created"]
    list_display = ["__str__", "updated", "complete", "killed", "task_id"]
    list_filter = [LimitGeoContextFilter]


admin.site.register(Snapshot, SnapshotAdmin)


class PictureInline(admin.TabularInline):
    model = Picture
    extra = 1


class MonumentAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    search_fields = ["label", "q_number"]
    list_filter = [
        "in_contest",
        LimitGeoContextFilter,
        "app_category",
        "data_categories",
        "start",
        "snapshot",
        MonumentAdminEntityFilter,
        "disabled",
        "current_commons_state",
    ]
    autocomplete_fields = ["region", "province", "municipality"]
    # inlines = [PictureInline]


admin.site.register(Monument, MonumentAdmin)


class AppCategoryAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    list_filter = [LimitGeoContextFilter]
    list_display = ["__str__", "geo_context", "icon", "sector", "order"]
    inlines = [TranslationInline]


admin.site.register(AppCategory, AppCategoryAdmin)


class ContestAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    list_filter = [LimitGeoContextFilter]


admin.site.register(Contest, ContestAdmin)

class LocalContestExceptionInline(admin.TabularInline):
    model = LocalContestException
    readonly_fields = ["label"]
    extra = 2


class LocalContestForm(forms.ModelForm):
    class Meta:
        model = LocalContest
        widgets = {
            "sparql": AceWidget(
                mode="sparql", theme="mono", width="80%", height="400px"
            ),
        }
        fields = "__all__"


class LocalContestAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    geo_context_field = "contest__geo_context"
    limited_field = "contest"
    limited_lookup_field = "geo_context__pk"

    list_filter = [LimitGeoContextContestFilter]
    list_display = ["__str__", "has_award"]
    autocomplete_fields = ["regions", "provinces", "municipalities"]
    inlines = [LocalContestExceptionInline]
    form = LocalContestForm


admin.site.register(LocalContest, LocalContestAdmin)


class WLMCategoryRulePredicateInline(admin.TabularInline):
    model = WLMCategoryRulePredicate
    extra = 1


class WLMCategoryRuleAdmin(LimitedGeoContextMixin, admin.ModelAdmin):

    def get_predicates(self, obj):
        return ", ".join([p.__str__() for p in obj.predicates.all()])

    list_display = ["__str__", "geo_context", "order", "app_category", "get_predicates"]
    list_filter = [LimitGeoContextFilter, "app_category"]
    inlines = [WLMCategoryRulePredicateInline]


admin.site.register(WLMCategoryRule, WLMCategoryRuleAdmin)


class IconAdmin(LimitedGeoContextMixin,admin.ModelAdmin):
    def icon_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(obj.icon.url)
        )

    def icon_white_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_white.url
            )
        )

    def icon_black_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_black.url
            )
        )

    def icon_color_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_color.url
            )
        )

    def icon_pin_many_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_pin_many.url
            )
        )

    def icon_pin_many_contest_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_pin_many_contest.url
            )
        )

    def icon_pin_some_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_pin_some.url
            )
        )

    def icon_pin_some_contest_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_pin_some_contest.url
            )
        )

    def icon_pin_none_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_pin_none.url
            )
        )

    def icon_pin_none_contest_image(self, obj):
        return format_html(
            '<img src="{}" style="width:60px; height: auto"/>'.format(
                obj.icon_pin_none_contest.url
            )
        )

    list_display = [
        "name",
        "icon_image",
        "icon_pin_many_image",
        "icon_pin_some_image",
        "icon_pin_none_image",
    ]
    readonly_fields = [
        "icon_image",
        "icon_white",
        "icon_white_image",
        "icon_black",
        "icon_black_image",
        "icon_color",
        "icon_color_image",
        "icon_pin_many",
        "icon_pin_many_image",
        "icon_pin_many_contest",
        "icon_pin_many_contest_image",
        "icon_pin_some",
        "icon_pin_some_image",
        "icon_pin_some_contest",
        "icon_pin_some_contest_image",
        "icon_pin_none",
        "icon_pin_none_image",
        "icon_pin_none_contest",
        "icon_pin_none_contest_image",
    ]

    list_filter = [LimitGeoContextFilter]


admin.site.register(Icon, IconAdmin)


class QuerySnapshotAdmin(admin.ModelAdmin):
    # exclude = ["payload", "processed_payload"]
    fields = [
        "snapshot",
        "query",
        "total_monuments",
        "done_monuments",
        "complete",
        "task_status",
    ]
    readonly_fields = ["created", "updated"]
    list_display = ["__str__", "complete", "task_status"]
    list_filter = ["query__geo_context", "query__label"]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.defer("payload")


admin.site.register(QuerySnapshot, QuerySnapshotAdmin)


class GeoContextIntroTextForm(forms.ModelForm):
    class Meta:
        model = GeoContextIntroText
        widgets = {
            "text": AceWidget(theme="mono", width="80%", height="200px"),
        }
        fields = ["text"]


class GeoContextIntroTextInline(nested_admin.NestedStackedInline):
    model = GeoContextIntroText
    form = GeoContextIntroTextForm
    readonly_fields = ["language"]

    extra = 0


class GeoContextDonationTextForm(forms.ModelForm):
    class Meta:
        model = GeoContextDonationText
        widgets = {
            "text": AceWidget(theme="mono", width="80%", height="200px"),
        }
        fields = ["text"]


class GeoContextDonationTextInline(nested_admin.NestedStackedInline):
    model = GeoContextDonationText
    form = GeoContextDonationTextForm
    readonly_fields = ["language"]

    extra = 0


class GeoContextForm(forms.ModelForm):

    class Meta:
        model = GeoContext
        widgets = {
            "regions_overpass_query": AceWidget(
                theme="mono", width="80%", height="200px"
            ),
            "provinces_overpass_query": AceWidget(
                theme="mono", width="80%", height="200px"
            ),
            "municipalities_overpass_query": AceWidget(
                theme="mono", width="80%", height="200px"
            ),
        }
        fields = "__all__"


class GeoContextNestedAdmin(nested_admin.NestedModelAdmin):
    model = GeoContext
    form = GeoContextForm
    inlines = [GeoContextIntroTextInline, GeoContextDonationTextInline]
    # inlines = [WLMCategoryRuleNestedInline]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.prefetch_related("intro_texts", "donation_texts")
        if request.user.is_superuser:
            return qs
        admins = GeoContextAdmin.objects.filter(user=request.user)
        return qs.filter(pk__in=[a.geo_context.pk for a in admins])


admin.site.register(GeoContext, GeoContextNestedAdmin)


class GeoContextAdminForm(forms.ModelForm):
    generate_password = forms.BooleanField(
        required=False, label=_("Generate or regenerate password"), initial=False
    )
    class Meta:
        model = GeoContextAdmin
        fields = "__all__"


class GeoContextAdminAdmin(LimitedGeoContextMixin, admin.ModelAdmin):
    list_filter = [LimitGeoContextFilter]
    search_fields = ["geo_context", "user__email"]
    list_display = ["geo_context", "user"]
    autocomplete_fields = ["user"]
    form = GeoContextAdminForm

    @transaction.atomic
    def save_model(self, request, obj, form, change):
        user = obj.user
        should_generate_password = False
        if form.cleaned_data.get("generate_password"):
            msg = f"A new password has been generated for user {user}: <b>{new_password}</b>"
            new_password = User.objects.make_random_password()
            should_generate_password = True

        if not user.has_usable_password():
            should_generate_password = True
            new_password = User.objects.make_random_password()
            msg = f"A password has been generated for user {user}: <b>{new_password}</b>"

        if should_generate_password:
            user.set_password(new_password)
            user.save()
            
            msg = mark_safe(msg)
            messages.add_message(request, messages.INFO, msg)
        super(GeoContextAdminAdmin, self).save_model(request, obj, form, change)


admin.site.register(GeoContextAdmin, GeoContextAdminAdmin)


class WLMCategoryRulePredicateNestedInline(nested_admin.NestedTabularInline):
    model = WLMCategoryRulePredicate
    extra = 1


class WLMCategoryRuleNestedInline(nested_admin.NestedStackedInline):
    model = WLMCategoryRule
    inlines = [WLMCategoryRulePredicateNestedInline]
    extra = 1


class QueryPlaceholderValueInline(admin.TabularInline):
    model = QueryPlaceholderValue
    extra = 1


class QueryPlaceholderAdmin(admin.ModelAdmin):
    inlines = [QueryPlaceholderValueInline]


admin.site.register(QueryPlaceholder, QueryPlaceholderAdmin)


admin.site.register(Language)


class TranslationTokenAdmin(admin.ModelAdmin):
    inlines = [TranslationInline]


admin.site.register(TranslationToken, TranslationTokenAdmin)


class TranslationAdmin(admin.ModelAdmin):
    list_display = ["__str__", "language", "translation_token", "app_category", "value"]
    list_filter = ["language"]
    search_fields = ["value"]
    readonly_fields = ["translation_token", "app_category", "language"]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Translation, TranslationAdmin)

class SimpleLogAdmin(admin.ModelAdmin):
    list_display = ["__str__", "created"]
    search_fields = ["message"]

admin.site.register(SimpleLog, SimpleLogAdmin)