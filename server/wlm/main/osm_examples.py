ITA_REGIONS = """
    [out:json];
    area["ISO3166-1"="IT"]->.boundaryarea;
    (
        relation["admin_level"="4"]["ref:ISTAT"]["boundary"="administrative"](area.boundaryarea);
    );
    out geom;
    """
