from django.core.management.base import BaseCommand, CommandError
from main.models import GeoContext
import json


class Command(BaseCommand):
    help = "Takes new snapshot"

    def add_arguments(self, parser):
        parser.add_argument("geo_context_label", type=str)
        parser.add_argument("output", type=str)

        

    def handle(self, *args, **options):

        try:
            geo_context = GeoContext.objects.get(label=options["geo_context_label"])
        except GeoContext.DoesNotExist:
            raise CommandError('Geo context "%s" does not exist' % options["geo_context_label"])
        
        fields = [
            "regions_overpass_query",  "regions_name_tag", "provinces_overpass_query", "provinces_name_tag", "municipalities_overpass_query", "municipalities_name_tag"
        ]
        
        data = {
            field: getattr(geo_context, field) for field in fields
        }
        with open(options["output"], "w") as f:
            json.dump(data, f)
        

        

        

        


# self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
# raise CommandError('Poll "%s" does not exist' % poll_id)
