from django.core.management.base import BaseCommand, CommandError
from main.models import GeoContext
import json


class Command(BaseCommand):
    help = "Takes new snapshot"

    def add_arguments(self, parser):
        parser.add_argument("geo_context_label", type=str)
        parser.add_argument("input", type=str)

        

    def handle(self, *args, **options):

        with open(options["input"], "r") as f:
            data = json.load(f)

        try:
            geo_context = GeoContext.objects.get(label=options["geo_context_label"])
        except GeoContext.DoesNotExist:
            raise CommandError('Geo context "%s" does not exist' % options["geo_context_label"])
        
        for item in data:
            setattr(geo_context, item, data[item])
        geo_context.save()
        