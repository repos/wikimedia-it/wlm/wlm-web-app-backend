from django.db import transaction
from main.models import GeoContext
from django.core.management.base import BaseCommand, CommandError
from main.osm_helpers import update_geocontext
from main.helpers import fix_duplicates_municipality_labels


class Command(BaseCommand):
    help = "Updates osm data for geo context"

    def add_arguments(self, parser):
        parser.add_argument("geo_context_label", type=str)
        parser.add_argument("--skip-municipality-names", action="store_true")
        parser.add_argument("--skip-province-names", action="store_true")
        parser.add_argument("--skip-region-names", action="store_true")
        parser.add_argument("--skip-municipalities", action="store_true")
        parser.add_argument("--skip-provinces", action="store_true")
        parser.add_argument("--skip-regions", action="store_true")

    @transaction.atomic
    def handle(self, *args, **options):
        try:
            geo_context = GeoContext.objects.get(label=options["geo_context_label"])
        except GeoContext.DoesNotExist:
            raise CommandError(
                'Geo context "%s" does not exist' % options["geo_context_label"]
            )

        update_geocontext(
            geo_context,
            skip_regions=options["skip_regions"],
            skip_provinces=options["skip_provinces"],
            skip_municipalities=options["skip_municipalities"],
            skip_municipality_names=options["skip_municipality_names"],
            skip_province_names=options["skip_province_names"],
            skip_region_names=options["skip_region_names"],
        )
        fix_duplicates_municipality_labels(geo_context)


# self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
# raise CommandError('Poll "%s" does not exist' % poll_id)
