from main.helpers import update_monument_pictures
from main.wiki_api import search_commons_cat
from django.core.management.base import BaseCommand
from main.models import Monument


class Command(BaseCommand):
    help = 'Takes new snapshot'

    def add_arguments(self, parser):
        parser.add_argument('id', type=int)
        parser.add_argument('--reset-pictures', action='store_true', help='Reset pictures', default=False)

    def handle(self, *args, **options):

        m = Monument.objects.get(pk=options['id'])
        update_monument_pictures(m.id, reset_pictures=options['reset_pictures'])

        
