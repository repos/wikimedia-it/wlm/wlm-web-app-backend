from django.core.management.base import BaseCommand, CommandError
from main.models import GeoContext, Monument
import json


class Command(BaseCommand):
    help = "Clears monuments for a geo context"

    def add_arguments(self, parser):
        parser.add_argument("geo_context_label", type=str)
        

    def handle(self, *args, **options):

        try:
            geo_context = GeoContext.objects.get(label=options["geo_context_label"])
        except GeoContext.DoesNotExist:
            raise CommandError('Geo context "%s" does not exist' % options["geo_context_label"])
        
        Monument.objects.filter(geo_context=geo_context).delete()
        