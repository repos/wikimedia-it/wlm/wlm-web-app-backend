from django.core.management.base import BaseCommand, CommandError
from main.models import GeoContext, Monument, Municipality
from main.helpers import fix_duplicates_municipality_labels
import logging

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Takes new snapshot"

    def add_arguments(self, parser):
        parser.add_argument("geo_context", type=str)

        

    def handle(self, *args, **options):

        geo_context = GeoContext.objects.get(label=options["geo_context"])
        
        app_category = geo_context.app_categories.filter(
            is_municipality=True
        )
        if not app_category.exists():
            raise CommandError("No municipality category found for this geo context")
        
        app_category = app_category.first()
        monuments = Monument.objects.filter(
            app_category=app_category,
            geo_context=geo_context
        )

        for monument in monuments:
            if monument.municipality:
                logger.info(f"Monument {monument} already has a municipality")
                continue
            try:
                municipality = Municipality.objects.get(
                    code = monument.q_number,
                    geo_context=geo_context
                )
                logger.info(f"Monument {monument} has municipality {municipality}")
                monument.municipality = municipality
                monument.province = municipality.province
                monument.region = municipality.region
                monument.save()
            except Municipality.DoesNotExist:
                logger.error(f"Monument {monument} has no municipality")
        

        

        

        


# self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
# raise CommandError('Poll "%s" does not exist' % poll_id)
