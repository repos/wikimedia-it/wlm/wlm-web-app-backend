from django.core.management.base import BaseCommand
from main.models import Snapshot, GeoContext

class Command(BaseCommand):
    help = 'Clean snapshots'

    def add_arguments(self, parser):
        parser.add_argument('--dry-run', action='store_true', help='Do not delete anything', default=False)
        parser.add_argument('--num-snapshots', type=int, help='Number of snapshots to keep', default=2)

    def handle(self, *args, **kwargs):
        """
        for each geocontext
        Keep the last two complete snapshots
        keep the last two not complete snapshots
        delete the rest
        """
        num_snapshots = kwargs['num_snapshots']
        if num_snapshots < 2:
            raise ValueError('Sorry, you must keep at least 2 snapshots per type')
        
        dry_run = kwargs['dry_run']

        self.stdout.write(self.style.SUCCESS('Starting to clean snapshots...'))
        self.stdout.write(self.style.WARNING(f'Keeping {num_snapshots} snapshots per type (complete/not complete)'))
        
        if dry_run:
            self.stdout.write(self.style.WARNING('Running in dry-run mode. Nothing will be deleted.'))

        for geocontext in GeoContext.objects.all():
            self.stdout.write(self.style.SUCCESS(f'Cleaning snapshots for {geocontext}'))
            snapshots = Snapshot.objects.filter(geo_context=geocontext).order_by('-created')
            complete_snapshots = snapshots.filter(complete=True)
            not_complete_snapshots = snapshots.filter(complete=False)
            
            if len(complete_snapshots) > num_snapshots:
                self.stdout.write(f'Keeping {num_snapshots} complete snapshots')
                for snapshot in complete_snapshots[2:]:
                    self.stdout.write(f'Deleting {snapshot}')
                    if not dry_run:
                        snapshot.delete()
            else:
                self.stdout.write(f'Keeping all complete snapshots ({len(complete_snapshots)})')
            
            if len(not_complete_snapshots) > num_snapshots:
                self.stdout.write(f'Keeping {num_snapshots} not complete snapshots')
                for snapshot in not_complete_snapshots[2:]:
                    self.stdout.write(f'Deleting {snapshot}')
                    if not dry_run:
                        snapshot.delete()
            else:
                self.stdout.write(f'Keeping all not complete snapshots ({len(not_complete_snapshots)})')

        
        # Add your snapshot cleaning logic here
        self.stdout.write(self.style.SUCCESS('Snapshots cleaned successfully.'))
        if dry_run:
            self.stdout.write(self.style.WARNING('Dry-run mode. No snapshots were deleted.'))