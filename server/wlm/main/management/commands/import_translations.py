from main.models import TranslationToken, Language, Translation
import json
from django.core.management.base import BaseCommand, CommandError




class Command(BaseCommand):
    help = 'Takes new snapshot'

    def add_arguments(self, parser):
        parser.add_argument('path', type=str)
        
        

    def handle(self, *args, **options):
        
        path = options['path']
        with open(path) as f:
            data = json.load(f)
        
        for lang in data:
            try:
                language = Language.objects.get(code=lang)
            except Language.DoesNotExist:
                continue
            
            translations = data[lang].get("translation", {}) 
            for token in translations:
                #ensure token exists
                value = translations[token]
                print(token, value)
                translation_token, created_token = TranslationToken.objects.get_or_create(code=token)
                translation, created_trans = Translation.objects.get_or_create(translation_token=translation_token, language=language, defaults={"value": value})
                translation.value = value
                translation.save()
                