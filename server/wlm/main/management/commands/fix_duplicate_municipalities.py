from django.core.management.base import BaseCommand, CommandError
from main.models import GeoContext
from main.helpers import fix_duplicates_municipality_labels


class Command(BaseCommand):
    help = "Takes new snapshot"

    def add_arguments(self, parser):
        parser.add_argument("geo_context", type=str)

        

    def handle(self, *args, **options):

        geo_context = GeoContext.objects.get(label=options["geo_context"])
        fix_duplicates_municipality_labels(geo_context)
        

        

        

        


# self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
# raise CommandError('Poll "%s" does not exist' % poll_id)
