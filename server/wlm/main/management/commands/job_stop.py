from django.core.management.base import BaseCommand, CommandError
from celery import current_app as celery_app
from main.models import Snapshot
from main.celery_helpers import kill_traced_tasks


class Command(BaseCommand):
    help = "Takes new snapshot"

    def add_arguments(self, parser):
        parser.add_argument("--killed", action="store_true", required=False, default=False)

    def handle(self, *args, **options):
        
        if options["killed"]:
            snaps = Snapshot.objects.filter(complete=False)            
        else:
            snaps = Snapshot.objects.filter(complete=False, killed=False)
            

        for snap in snaps:
            print("Terminating task", snap.task_id)
            kill_traced_tasks(snap.task_id)
            snap.killed = True
            snap.save()
            
        

# self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
# raise CommandError('Poll "%s" does not exist' % poll_id)
