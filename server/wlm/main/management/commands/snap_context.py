from django.core.management.base import BaseCommand, CommandError
from main.models import GeoContext
from main.tasks import snap_context


class Command(BaseCommand):
    help = "Takes new snapshot"

    def add_arguments(self, parser):
        parser.add_argument("geo_context", type=str)

        parser.add_argument("--skip-pictures", action="store_true", required=False)
        parser.add_argument("--skip-geo", action="store_true", required=False)
        parser.add_argument("--category-only", action="store_true", required=False)
        # parser.add_argument('--dry-run', action='store_true', required=False)
        parser.add_argument("--force-restart", action="store_true", required=False)
        parser.add_argument("--skip-commons", action="store_true", required=False)

    def handle(self, *args, **options):

        perform_commons = not options["skip_commons"]

        try:
            geo_context = GeoContext.objects.get(label=options["geo_context"])
        except GeoContext.DoesNotExist:
            raise CommandError("Geo context does not exist")

        results = snap_context.delay(geo_context.id, perform_commons=perform_commons)
        results.get()
        return results.id
        

        

        

        


# self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
# raise CommandError('Poll "%s" does not exist' % poll_id)
