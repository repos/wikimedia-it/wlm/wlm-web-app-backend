from django.core.management.base import BaseCommand, CommandParser
from main.scraping import execute_sparql_query_template

class Command(BaseCommand):
    help = 'Your command description goes here'

    def add_arguments(self, parser):
        parser.add_argument('sparql_query', type=str)
        parser.add_argument('--pl', type=str,  action='append')
        parser.add_argument('--pv', type=str,  action='append', nargs='+')
        
        

    def handle(self, *args, **options):
        # Your code logic goes here
        sparql_query = options['sparql_query']
        print(options['pl'])
        print(options['pv'])

        if len(options['pl']) != len(options['pv']):
            print("placeholders and placeholders values must be the same length")
            return

        with open(sparql_query) as f:
            query = f.read()

        #print(query)
        results = execute_sparql_query_template(query, options['pl'], options['pv'])
        print(len(results))