from django.core.management.base import BaseCommand, CommandError
from main.models import GeoContext, Monument, Municipality
from main.helpers import fix_duplicates_municipality_labels
import logging

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Assings the other monuments category to monuments without category for a given geo context"

    def add_arguments(self, parser):
        parser.add_argument("geo_context", type=str)

        

    def handle(self, *args, **options):

        geo_context = GeoContext.objects.get(label=options["geo_context"])
        
        app_category_other = geo_context.app_categories.filter(
            is_other_monuments=True
        )
        if not app_category_other.exists():
            raise CommandError("No other monuments category found for this geo context")
        
        app_category = app_category_other.first()
        monuments = Monument.objects.filter(
            app_category=None,
            geo_context=geo_context
        )
        monuments.update(app_category=app_category, app_category_cat=app_category)

        