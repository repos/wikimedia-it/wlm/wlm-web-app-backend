import pathlib
from cairosvg import svg2png
import io
from PIL import Image, ImageDraw, ImageFilter
import xml.etree.ElementTree as ET

BASE_IMAGES_PATH = pathlib.Path(__file__).parent.resolve() / "base_icons"

def svg_on_png(
        svg_content: bytes,
        png_content: bytes,
):
    pass


PHOTO_RANGES = ['none', 'some', 'many']


def make_color(svg_content: bytes, fill: str, stroke: str):
    root = ET.fromstring(svg_content)
    root.set("xmlns", "http://www.w3.org/2000/svg")
    style = ET.Element("style")
    
    if not stroke:
        style.text = "* {fill: "+fill+";}"
    else:
        style.text = "* {fill: "+fill+";  stroke: " + stroke + "; stroke-width: 0.5px;}"
    
    root.insert(0, style)
    svg_content = ET.tostring(root, encoding='utf-8')

    out_file = io.BytesIO()
    svg2png(bytestring=svg_content,write_to=out_file, output_height=80, output_width=80, background_color="rgba(0,0,0,0)")
    return out_file



def make_pin(svg_content: bytes, range: str, in_contest:bool): 
    if range not in PHOTO_RANGES:
        raise ValueError(f"range must be one of {PHOTO_RANGES}")
    
    background_path = BASE_IMAGES_PATH / f"box_{range}.png"
    
    print(BASE_IMAGES_PATH)
    print(svg_content)
    out_file = io.BytesIO()

    root = ET.fromstring(svg_content)
    root.set("xmlns", "http://www.w3.org/2000/svg")

    
    style = ET.Element("style")
    if in_contest:
        style.text = "* {fill: white;}"
    else:
        style.text = "* {fill: black; stroke: white; stroke-width: 0.5px;}"

    #insert the style node
    root.insert(0, style)
    #convert the tree back to a string
    svg_content = ET.tostring(root, encoding='utf-8')
    
    svg2png(bytestring=svg_content,write_to=out_file, output_height=80, output_width=80, background_color="rgba(0,0,0,0)")
    
    svg_image = Image.open(out_file)
    background = Image.open(background_path)

    background.paste(svg_image, [34, 10], svg_image)
    img_byte_arr = io.BytesIO()
    background.save(img_byte_arr, format='PNG')
    return img_byte_arr
