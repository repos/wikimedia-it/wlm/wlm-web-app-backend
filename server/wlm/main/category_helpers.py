from main.models import AppCategory, WLMCategoryRule
import logging

logger = logging.getLogger(__name__)

def categorize_monument(monument_data, data_categories, categorize_for_app, app_category_other=None, rules=[]):
    #logger.info(f"categorizing: Processing monument {monument_data}")

    if data_categories:
        monument_data["categories"] = [x.pk for x in data_categories]

    categorized = False
    #Process rules    
    if categorize_for_app:
        for rule in rules:
            if rule.matches(monument_data):
                monument_data['app_category_cat'] = rule.app_category.pk
                # forcing this categpory eventual categorization done by data categories (code below)
                # useful if "non-wlm queries" run faster and assign non-compliat categories
                categorized = True
                break
    else:
        if not categorized:
            for data_category in data_categories:
                if data_category.app_category:
                    monument_data['app_category_data'] = data_category.app_category.pk
                    break
        
    #FALLBACK TO ASSIGN "OTHER" CATEGORY
    if not categorized and app_category_other:
        monument_data['app_category_cat'] = app_category_other.pk

    return monument_data