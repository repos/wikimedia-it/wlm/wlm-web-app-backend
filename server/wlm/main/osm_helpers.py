import overpy
import geojson
import requests
import shapely.geometry as geometry
from shapely.ops import linemerge, unary_union, polygonize
from django.contrib.gis.geos import GEOSGeometry, Point
from main.models import Region, Province, Municipality
from wlm_app_api.helpers import get_property_value
import logging

logger = logging.getLogger("wlm")

overpass_url = "https://overpass-api.de/api/interpreter"


def run_overpass_query(query):
    request = requests.post(overpass_url, data=query)
    overpass_data = request.json()
    elements = overpass_data["elements"]
    return elements


def overpass_centroid(overpass_element):
    members = overpass_element["members"]
    candidates = [
        x for x in members if x["type"] == "node" and x["role"] == "admin_centre"
    ]
    if candidates:
        node = candidates[0]
        return Point(node["lon"], node["lat"])
    return None


def overpass_to_multipolygon(overpass_element):

    members = overpass_element["members"]
    lines = []
    for member in members:
        if member["type"] == "way" and member["role"] == "outer":
            coordinates = [[node["lon"], node["lat"]] for node in member["geometry"]]
            line = geometry.LineString(coordinates)
            lines.append(line)

    merged = linemerge([*lines])  # merge LineStrings
    borders = unary_union(merged)  # linestrings to a MultiLineString
    polygons = list(polygonize(borders))
    multi_poly = unary_union(polygons)
    if multi_poly.geom_type == "Polygon":
        multi_poly = geometry.MultiPolygon([multi_poly])

    geom = GEOSGeometry(multi_poly.wkt)

    return geom


def update_geocontext(
    geo_context,
    skip_regions=False,
    skip_provinces=False,
    skip_municipalities=False,
    skip_municipality_names=False,
    skip_province_names=False,
    skip_region_names=False,
):
    # regions
    regions_query = geo_context.regions_overpass_query
    if regions_query and not skip_regions:
        logger.info("regions")
        regions_name_tag = geo_context.regions_name_tag
        done_regions_codes = []
        regions = run_overpass_query(regions_query)
        logger.info("regions query ok")
        for region_item in regions:
            geom = overpass_to_multipolygon(region_item)
            centroid = overpass_centroid(region_item)
            if geom and centroid is None:
                centroid = geom.centroid
            name = region_item["tags"].get(
                regions_name_tag, region_item["tags"].get("name")
            )
            code = region_item["tags"].get("wikidata", "")
            logger.info(name)
            try:
                region = Region.objects.get(geo_context=geo_context, code=code)
                region.poly = geom
                if not skip_region_names:
                    region.name = name
                region.centroid = centroid
                region.save()
            except Region.DoesNotExist:
                Region.objects.create(
                    geo_context=geo_context,
                    name=name,
                    code=code,
                    poly=geom,
                    centroid=centroid,
                )
            done_regions_codes.append(code)

        geo_context.regions.exclude(code__in=done_regions_codes).delete()
        del regions

    # provinces
    provinces_query = geo_context.provinces_overpass_query
    if provinces_query and not skip_provinces:
        logger.info("provinces")
        provinces_name_tag = geo_context.provinces_name_tag
        done_provinces_codes = []
        provinces = run_overpass_query(provinces_query)
        logger.info("provinces query ok")
        for province_item in provinces:
            geom = overpass_to_multipolygon(province_item)
            name = province_item["tags"].get(
                provinces_name_tag, province_item["tags"].get("name")
            )
            code = province_item["tags"].get("wikidata", "")
            centroid = overpass_centroid(province_item)
            if geom and centroid is None:
                centroid = geom.centroid
            logger.info(name)

            try:
                province = Province.objects.get(geo_context=geo_context, code=code)
                province.poly = geom
                if not skip_province_names:
                    province.name = name
                province.centroid = centroid
                province.save()
            except Province.DoesNotExist:
                province = Province.objects.create(
                    geo_context=geo_context,
                    name=name,
                    code=code,
                    poly=geom,
                    centroid=centroid,
                )
            done_provinces_codes.append(code)

            # get region with spatial lookup
            region = Region.objects.filter(poly__contains=centroid).first()
            if region:
                province.region = region
                province.save()
            else:
                logger.warning(f"Region not found for {name}")

        geo_context.provinces.exclude(code__in=done_provinces_codes).delete()
        del provinces

    # municipalities
    municipalities_query = geo_context.municipalities_overpass_query
    
    
    if municipalities_query and not skip_municipalities:

        logger.info("municipalities")
        municipalities_name_tag = geo_context.municipalities_name_tag
        done_municipalities_codes = []
        municipalities = run_overpass_query(municipalities_query)
        logger.info("municipalities query ok")

        for municipality_item in municipalities:
            geom = overpass_to_multipolygon(municipality_item)
            name = municipality_item["tags"].get(
                municipalities_name_tag, municipality_item["tags"].get("name")
            )
            code = municipality_item["tags"].get("wikidata", "")
            centroid = overpass_centroid(municipality_item)
            if geom and centroid is None:
                centroid = geom.centroid
            logger.info(name)
            try:
                municipality = Municipality.objects.get(
                    geo_context=geo_context, code=code
                )
                municipality.poly = geom
                if not skip_municipality_names:
                    municipality.name = name
                municipality.centroid = centroid
                municipality.save()
            except Municipality.DoesNotExist:
                municipality = Municipality.objects.create(
                    geo_context=geo_context,
                    name=name,
                    code=code,
                    poly=geom,
                    centroid=centroid,
                )
            done_municipalities_codes.append(code)

            # get province with spatial lookup
            province = Province.objects.filter(poly__contains=centroid).first()
            if province:
                municipality.province = province
                municipality.region = province.region
                municipality.save()
            else:
                logger.warning(f"Province not found for {name}")

        geo_context.municipalities.exclude(code__in=done_municipalities_codes).delete()
