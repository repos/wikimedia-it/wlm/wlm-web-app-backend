#!/bin/bash

celery -A wlm worker --loglevel INFO  -P eventlet -c 64 --queues=celery,update_monuments,execute_sparql,update_commons
