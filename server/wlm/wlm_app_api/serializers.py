import requests
from pathlib import Path
from main.models import (
    GeoContext,
    GeoContextIntroText,
    GeoContextDonationText,
    Monument,
    Picture,
    Contest,
    AppCategory,
    Icon,
    Municipality,
    Snapshot,
    QuerySnapshot,
    Translation,
    Language,
)
from rest_framework import serializers
from rest_framework_gis.serializers import GeometryField, GeoFeatureModelSerializer
from django.db import models
from .helpers import get_upload_categories
from django.utils import timezone
import json
import markdown


class GeoContextSnapshotSerializer(serializers.ModelSerializer):

    query_status = serializers.SerializerMethodField()

    def get_query_status(self, obj):
        queries = obj.geo_context.sparql_queries.all()
        query_snapshots = QuerySnapshot.objects.filter(
            query__in=queries, snapshot=obj
        ).defer("query", "payload")
        query_snapshots_by_query_id = {qs.query_id: qs for qs in query_snapshots}
        query_status = []
        for query in queries:
            query_snapshot = query_snapshots_by_query_id.get(query.id)
            query_record = {
                "label": query.label,
                "total_monuments": 0,
            }
            if query_snapshot:
                query_record["total_monuments"] = query_snapshot.total_monuments

            query_status.append(query_record)
        return query_status

    class Meta:
        model = Snapshot
        fields = [
            "id",
            "updated",
            "created",
            "complete",
            "killed",
            "query_status",
            "done_monuments",
            "total_monuments",
            "task_status",
        ]


class GeoContextLastSnapshotsSerializer(serializers.Serializer):
    last_snapshot_done = GeoContextSnapshotSerializer(required=False, read_only=True)
    last_snapshot_running = GeoContextSnapshotSerializer(required=False, read_only=True)
    snapshot_queued = serializers.BooleanField(read_only=True)


class IconSerializer(serializers.ModelSerializer):
    class Meta:
        model = Icon
        fields = "__all__"


class AppCategorySerializer(serializers.ModelSerializer):
    icon = IconSerializer(read_only=True)

    class Meta:
        model = AppCategory
        fields = ["id", "name", "order", "icon", "is_municipality"]


class GeoContextMunicipalitySerializer(serializers.ModelSerializer):

    region_name = serializers.CharField(source="region.name", read_only=True)
    province_name = serializers.CharField(source="province.name", read_only=True)

    class Meta:
        model = Municipality
        fields = ["name", "code", "centroid", "province_name", "region_name", "id"]


class NextContestSerializer(serializers.ModelSerializer):

    active = serializers.SerializerMethodField()

    def get_active(self, obj):
        tz = obj.geo_context.timezone_name
        active_contests = Contest.get_active(tz_name=tz)
        return obj in active_contests

    class Meta:
        model = Contest
        fields = "__all__"


class GeoContextSerializer(serializers.ModelSerializer):
    app_categories = AppCategorySerializer(many=True)
    num_monuments = serializers.IntegerField(read_only=True)
    next_contest = serializers.SerializerMethodField()
    intro_texts = serializers.SerializerMethodField()
    donation_texts = serializers.SerializerMethodField()

    def get_intro_texts(self, obj):
        intro_texts = GeoContextIntroText.objects.filter(geo_context=obj)
        out = {}
        for text in intro_texts:
            out[text.language.code] = text.intro_text_html()
        return out

    def get_donation_texts(self, obj):
        donation_texts = GeoContextDonationText.objects.filter(geo_context=obj)
        out = {}
        for text in donation_texts:
            out[text.language.code] = text.donation_text_html()
        return out

    def get_next_contest(self, obj):
        next_context = obj.next_contest
        if next_context:
            contest = Contest.objects.get(pk=next_context)
            return NextContestSerializer(contest).data

    class Meta:
        model = GeoContext
        fields = [
            "id",
            "label",
            "description",
            "country_code",
            "app_domain",
            "monument_definition",
            "app_categories",
            "language_code",
            "flag",
            "centroid",
            "zoom_level",
            "num_monuments",
            "next_contest",
            "intro_texts",
            "donation_texts",
            "enable_donations_text",
            "donations_popup_probability",
        ]


class MonumentAppListSerializer(serializers.ModelSerializer):
    municipality_label = serializers.CharField(
        source="municipality.name", read_only=True
    )
    distance = serializers.FloatField(
        read_only=True, required=False, source="distance.km"
    )

    class Meta:
        model = Monument
        fields = [
            "id",
            "label",
            "municipality_label",
            "municipality",
            "pictures_wlm_count",
            "pictures_count",
            "in_contest",
            "app_category",
            "distance",
            "address",
            "location",
            "position",
            "q_number",
            "disabled",
        ]


class PictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Picture
        fields = "__all__"


class MonumentAppDetailSerialier(serializers.ModelSerializer):
    pictures = serializers.SerializerMethodField()
    cover_picture = serializers.SerializerMethodField()
    counts_comune_by_app_category = serializers.SerializerMethodField()
    municipality_label = serializers.CharField(
        source="municipality.name", read_only=True
    )
    province_label = serializers.CharField(
        source="municipality.province.name", read_only=True
    )
    distance = serializers.FloatField(read_only=True, required=False)
    categories_urls = serializers.SerializerMethodField()
    parent_monuments = serializers.SerializerMethodField()
    children_monuments = serializers.SerializerMethodField()

    def get_parent_monuments(self, obj):
        parent_monuments_instances = []
        if obj.data.get("parent_n") and type(obj.data.get("parent_n")) == list:
            parent_monuments_instances = Monument.objects.filter(
                geo_context=obj.geo_context, q_number__in=obj.data.get("parent_n")
            )
        return MonumentAppListSerializer(parent_monuments_instances, many=True).data

    def get_children_monuments(self, obj):
        children_monuments_instances = []
        if obj.data.get("children_n") and type(obj.data.get("children_n")) == list:
            children_monuments_instances = Monument.objects.filter(
                geo_context=obj.geo_context, q_number__in=obj.data.get("children_n")
            )
        return MonumentAppListSerializer(children_monuments_instances, many=True).data

    def get_pictures(self, obj):
        pictures = (
            obj.pictures.all()
            .annotate(
                relevant_score=models.Case(
                    models.When(is_relevant=True, then=1),
                    default=0,
                    output_field=models.IntegerField(),
                )
            )
            .order_by("-relevant_score", "-image_date")
        )

        return PictureSerializer(pictures, many=True).data

    def get_cover_picture(self, obj):
        picture = obj.pictures.first()
        if picture:
            return PictureSerializer(picture).data

    def get_counts_comune_by_app_category(self, obj):
        app_category = obj.app_category
        if app_category:
            if app_category.is_municipality:
                monuments_by_category = (
                    Monument.objects.filter(municipality=obj.municipality)
                    .exclude(app_category__is_municipality=True)
                    .values("app_category__name", "app_category__id")
                    .annotate(count=models.Count("app_category__name"))
                    .order_by("app_category__name")
                )
                return monuments_by_category
            else:
                return None
        else:
            return None

    def get_categories_urls(self, obj):
        request = self.context.get("request")
        if request:
            platform = request.query_params.get("platform", "desktop")
        else:
            platform = "desktop"
        return get_upload_categories(obj, platform=platform)

    class Meta:
        model = Monument
        fields = "__all__"


class ClusterSerializer(serializers.Serializer):
    position = serializers.SerializerMethodField()

    ids = serializers.IntegerField()
    cid = serializers.IntegerField()

    def get_position(self, obj):
        return json.loads(obj["position"])


class ClusterGeoSerializer(serializers.Serializer):
    position = GeometryField()
    properties = serializers.DictField(required=False)

    ids = serializers.ListField(child=serializers.IntegerField())
    cid = serializers.IntegerField()


class UploadImageSerializer(serializers.Serializer):
    image = serializers.ImageField()
    title = serializers.CharField()
    description = serializers.CharField(required=False)
    date = serializers.DateField()
    monument_id = serializers.CharField()

    def validate_title(self, value):
        candidates = "/\;"
        for x in candidates:
            if x in value:
                raise serializers.ValidationError(
                    "Il titolo contiene caratteri non validi: " + x
                )
        return value

    def validate(self, attrs):
        out = super().validate(attrs)
        image = out["image"]
        title = out["title"]
        ext = Path(image.name).suffix
        title = f"File:{title}{ext}"

        url = f"https://commons.wikimedia.org/wiki/{title}"
        r = requests.head(url)
        if r.status_code == 200:
            raise serializers.ValidationError(
                "Esiste già un file con questo nome su Wikimedia Commons"
            )

        return out


class UploadImagesSerializer(serializers.Serializer):
    images = UploadImageSerializer(many=True)
    platform = serializers.CharField(required=False, default="desktop")


class ContestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contest
        fields = "__all__"


class MonumentGeoSerializer(GeoFeatureModelSerializer):
    ids = serializers.IntegerField(read_only=True)

    # pos = GeometryField(required=False)

    class Meta:
        model = Monument
        fields = "__all__"
        geo_field = "position"


class TranslationSerializer(serializers.ModelSerializer):

    key = serializers.SerializerMethodField()

    def get_key(self, obj):
        if obj.translation_token:
            return obj.translation_token.code
        if obj.app_category:
            return obj.app_category.name

    class Meta:
        model = Translation
        fields = ["key"]


class LanguageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Language
        fields = "__all__"



class StartSnapshotSerializer(serializers.Serializer):
    perform_commons = serializers.BooleanField(default=True)