import requests
import re
import datetime
from itertools import product
from main.helpers import execute_query, monument_prop, format_monument
from main.models import Monument, LocalContest, AppCategory
from django.core.cache import cache
from retry import retry
import logging
import datetime
from django.utils import timezone

logger = logging.getLogger(__name__)

def get_other_category_pk(geo_context_id):
    # try to get from cache
    cache_key = f"other_category_{geo_context_id}"
    category_pk = cache.get(cache_key)
    if category_pk:
        return category_pk

    category = AppCategory.objects.filter(is_other_monuments=True, geo_context_id=geo_context_id).first()
    if category:
        cache.set(cache_key, category.pk)
        return category.pk

    return None


def normalize_value(value):
    v = str(value)
    if v == "" or v.startswith("_:"):
        return None
    else:
        return v


SPARQL_MONUMENT = """
SELECT DISTINCT  ?item ?itemLabel ?itemDescription ?coords ?wlmid ?image ?sitelink ?commons ?regioneLabel ?enddate ?unit ?unitLabel ?address ?approvedby ?endorsedby ?year ?instanceof
    WHERE {

      VALUES ?item { wd:%s } .
      
      ?item p:P2186 ?wlmst .
      ?wlmst ps:P2186 ?wlmid .
      OPTIONAL { ?wlmst pq:P790 ?approvedby . }
      OPTIONAL { ?wlmst pq:P8001 ?endorsedby . }

      ?item wdt:P17 wd:Q38 . 
      ?item wdt:P131 ?unit .
      
      MINUS {?item wdt:P31 wd:Q747074.}
      MINUS {?item wdt:P31 wd:Q954172.}
      MINUS {?item wdt:P31 wd:Q1549591.}
      
      OPTIONAL {?item wdt:P31 ?instanceof }
      OPTIONAL {?item wdt:P625 ?coords. }
      OPTIONAL { ?wlmst pqv:P582 [ wikibase:timeValue ?enddate ] .}
      OPTIONAL { ?wlmst pqv:P585 [ wikibase:timeValue ?year ] .}
      OPTIONAL { ?item wdt:P373 ?commons. }
      OPTIONAL { ?item wdt:P18 ?image. }
      OPTIONAL { ?item wdt:P6375 ?address.}
      OPTIONAL {?sitelink schema:isPartOf <https://it.wikipedia.org/>;schema:about ?item. }
      VALUES ?typeRegion { wd:Q16110 wd:Q1710033 }.

      ?item wdt:P131* ?regione.
      ?regione wdt:P31 ?typeRegion.

      # esclude i monumenti che hanno una data di inizio successiva al termine del concorso
      MINUS {
        ?wlmst pqv:P580 [ wikibase:timeValue ?start ; wikibase:timePrecision ?sprec ] .
        FILTER (
          # precisione 9 è anno
          ( ?sprec >  9 && ?start >= "2023-10-01T00:00:00+00:00"^^xsd:dateTime ) ||
          ( ?sprec < 10 && ?start >= "2023-01-01T00:00:00+00:00"^^xsd:dateTime )
        )
      }
      
      SERVICE wikibase:label { bd:serviceParam wikibase:language "it" }
      }

      LIMIT 1

"""

@retry(tries=3, delay=0)
def get_monument_data(q_number):
    monument_data = execute_query(SPARQL_MONUMENT % q_number)
    if len(monument_data["results"]["bindings"]) == 0:
        return None
    monument_data = monument_data["results"]["bindings"][0]
    monument_data = format_monument(monument_data)

    data = {
        "item": monument_prop(monument_data, "item"),
        "regione": monument_prop(monument_data, "regioneLabel"),
        "is_religious": bool(monument_prop(monument_data, "endorsedby")),
        "city_item": normalize_value(monument_prop(monument_data, "unit")),
    }

    return data


@retry(tries=5, delay=10)
def get_property_value(q_number, property, parse_q_number=False, multiple=False):
    location_query = f"""
        SELECT ?item ?out
        WHERE
        {{
            ?item wdt:{property} ?out .
            FILTER(?item = wd:{q_number})
        }}
        """
    try:
        location_data = execute_query(location_query)
    except Exception as e:
        return None
    
    if len(location_data["results"]["bindings"]) > 0:
        out = [x["out"]["value"] for x in location_data["results"]["bindings"]]
        # clean q_numbers
        if parse_q_number:
            for i, item in enumerate(out):
                if "http://www.wikidata.org/entity/" in item:
                    item = item.replace("http://www.wikidata.org/entity/", "")
                if "http://www.wikidata.org/entity/statement/" in item:
                    item = item.replace("http://www.wikidata.org/entity/statement/", "")
                    item = item.split("-")[0]
                out[i] = item
        if not multiple:
            return out[0]
        return out
    return None


def get_wikidata_label(q_number, lang="en"):
    label_query = f"""
        SELECT ?item ?itemLabel
        WHERE
        {{
            VALUES ?item {{ wd:{q_number} }}
            SERVICE wikibase:label {{ bd:serviceParam wikibase:language "[AUTO_LANGUAGE], {lang}". }}
        }}
        """
    label_data = execute_query(label_query)
    if len(label_data["results"]["bindings"]) > 0:
        return label_data["results"]["bindings"][0]["itemLabel"]["value"]
    return None


def get_commons_category(monument):
    #Se la proprieta P373 (Commons category) del monumento è definita allora utilizzare il valore di P373
    if monument.data.get("commonsCat"):
        return monument.data.get("commonsCat")
    
    #altrimenti se la proprietà P276 (location) del moumento e la P373 di P276 sono settate allora utilizzare il valore di P373 di P276 
    if monument.location:
        commons_cat = get_property_value(monument.location, "P373")
        if commons_cat:
            return commons_cat

    #altrimenti se la proprietà P373 della proprieta P131 (administrative center) del moumento è settata allora utilizzare il valore di P373 di P131
    if monument.admin_entity:
        commons_cat = get_property_value(monument.admin_entity, "P373")
        if commons_cat:
            return commons_cat
        
    # last try: if the monument has a municipaliyu set, look for p373 of the municipality
    if monument.municipality:
        commons_cat = get_property_value(monument.municipality.code, "P373")
        if commons_cat:
            return commons_cat
        
    return None


def get_sparql_municipalities(local_contest):
    if local_contest.results:
        return local_contest.results
    sparql = local_contest.sparql
    municipalities = execute_query(sparql)
    municipalities = municipalities["results"]["bindings"]
    municipalities = [format_monument(x) for x in municipalities]
    municipalities = [monument_prop(x, "item") for x in municipalities]
    local_contest.results = municipalities
    local_contest.save()
    return municipalities


def get_upload_categories(monument, platform="desktop"):

    country = monument.geo_context.commons_category_label
    country_code = monument.geo_context.country_code
    app_domain = monument.geo_context.app_domain
    now = timezone.now()
    today = now.date()
    year = today.year
    
    commons_category = get_commons_category(monument)
    
    #removed - generated by templates
    #stat_category = f"Files uploaded via webapp Wiki Loves Monuments {country} - {platform} in {year}"
    #national_category = f"Images from Wiki Loves Monuments {year} in {country}"

    templates = [
        f"{{{{Load via app {app_domain}|year={year}|source={platform}}}}}",
        f"{{{{Wiki Loves Monuments {year}|{country_code}}}}}"
    ]

    non_wlm_categories = []
    if commons_category:
        non_wlm_categories.append(commons_category)

    wlm_categories = [x for x in non_wlm_categories]
    
    if monument.app_category and monument.app_category.sector:
        wlm_categories.append(f"Images from Wiki Loves Monuments {year} in {country} - {monument.app_category.sector}")

    #local contests
    local_contests = LocalContest.objects.filter(
        contest__geo_context=monument.geo_context,
    ).prefetch_related("regions", "provinces", "municipalities", "exceptions")

    local_contests_categories = []
    selected_local_contests = []
    if monument.admin_entity or (monument.app_category and monument.app_category.is_municipality) or monument.municipality:
        admin_entity = monument.admin_entity
        if monument.app_category and monument.app_category.is_municipality:
            admin_entity = monument.q_number
        if monument.municipality:
            admin_entity = monument.municipality.code
        
        for local_contest in local_contests:

            matching_exceptions = local_contest.exceptions.filter(
                q_number=monument.q_number
            ).exists()
            if matching_exceptions:
                continue

            local_contest_municipalities = []
            
            if local_contest.sparql:
                local_contest_municipalities=get_sparql_municipalities(local_contest)   
            
            if local_contest.regions.all().exists():
                for region in local_contest.regions.all():
                    local_contest_municipalities += list(
                        region.municipalities.values_list('code', flat=True)
                    )
                
            
            if local_contest.provinces.all().exists():
                for province in local_contest.provinces.all():
                    local_contest_municipalities += list(
                        province.municipalities.values_list('code', flat=True)
                    )

            local_contest_municipalities += list(
                local_contest.municipalities.values_list('code', flat=True)
            )
            
            if admin_entity in local_contest_municipalities: 
                contest_cat =  f"Images from Wiki Loves Monuments {year} in {country} - {local_contest.label}"
                local_contests_categories.append(contest_cat)
                selected_local_contests.append(local_contest)

                if monument.app_category and monument.app_category.sector:
                    contest_cat_sector =  f"Images from Wiki Loves Monuments {year} in {country} - {local_contest.label} - {monument.app_category.sector}"    
                    local_contests_categories.append(contest_cat_sector)
    
        has_awards = False
            
        if len(local_contests_categories) > 0:
            wlm_categories.extend(local_contests_categories)
            for contest in selected_local_contests:
                if contest.has_award:
                    has_awards = True
                    break
        
        #adding the "withoud local award" commons category only if there are local contests and none of them has an award
        if not has_awards and len(local_contests_categories) > 0:
            wlm_categories.append(f"Images from Wiki Loves Monuments {year} in {country} - without local award")
            

    return {
        "wlm_categories": wlm_categories,
        "non_wlm_categories": non_wlm_categories,
        "templates": templates,
        
    }


SPARQL_MONUMENT_UPDATE = """
SELECT DISTINCT

  ?mon ?monLabel
  ?locationLabel
  ?article ?commonsCat ?geo ?wlm

  (group_concat(DISTINCT ?instanceOf; separator=";") as ?instanceOf_n)
  (group_concat(DISTINCT ?parent; separator=";") as ?parent_n)
  (group_concat(DISTINCT ?children; separator=";") as ?children_n)
  (group_concat(DISTINCT ?place; separator=";") as ?place_n)
  (group_concat(DISTINCT ?start; separator=";") as ?start_n)
  (group_concat(DISTINCT ?end; separator=";") as ?end_n)
  (group_concat(DISTINCT ?approvedBy; separator=";") as ?approvedBy_n)
  (group_concat(DISTINCT ?endorsedBy; separator=";") as ?endorsedBy_n)
  (group_concat(DISTINCT ?accreditedBy; separator=";") as ?accreditedBy_n)
  (group_concat(DISTINCT ?relevantImage; separator=";") as ?relevantImage_n)
  (SAMPLE(?address) as ?address)
  (SAMPLE(?place) as ?adminEntity)
  (SAMPLE(?location) as ?location)

WHERE {
  
  FILTER(?mon = wd:%s).

  SERVICE wikibase:label { bd:serviceParam wikibase:language "%s, [AUTO_LANGUAGE]". }

  
  ?wlms ps:P2186 ?wlm.
  ?mon wdt:P31 ?instanceOf .

  OPTIONAL { ?wlms pq:P580 ?start . }
  OPTIONAL { ?wlms pq:P582 ?end . }
  OPTIONAL { ?wlms pq:P790 ?approvedBy. }
  OPTIONAL { ?wlms pq:P8001 ?endorsedBy. }
  OPTIONAL { ?wlms pq:P5514 ?accreditedBy. }
  OPTIONAL { ?mon wdt:P625 ?geo . }
  OPTIONAL { ?mon wdt:P131 ?place . }
  OPTIONAL { ?mon wdt:P361 ?parent }
  OPTIONAL { ?mon wdt:P527 ?children }
  OPTIONAL { ?mon wdt:P18 ?relevantImage . }
  OPTIONAL { ?mon wdt:P373 ?commonsCat . }
  OPTIONAL { ?mon wdt:P276 ?location . }
  OPTIONAL {
    ?article schema:about ?mon ;
    schema:isPartOf <https://it.wikipedia.org/> .
  }
  OPTIONAL { ?mon wdt:P6375 ?address . }

}

GROUP BY ?mon ?monLabel ?locationLabel ?article ?commonsCat ?geo ?wlm

"""

def get_monument_data_update(q_number, geo_context):

    #finds the right query to be executed
    #should be based on the geo_context
    queries = geo_context.sparql_queries.filter(
        categorize_for_app=True
    )
    if not queries.exists():
        return None
    
    PLACEHOLDER = "#-FILTER-MONUMENT-PLACEHOLDER-#"
    
    candidate_query = None
    for qu in queries:
        
        if qu.sparql.find(PLACEHOLDER) > -1:
            candidate_query = qu
            break

    if not candidate_query:
        return None

    filtered_sparql = candidate_query.sparql.replace(PLACEHOLDER, f"FILTER(?mon = wd:{q_number}).")            
    
    # sparql data fetching
    placeholders = []
    placeholders_values = []
    if candidate_query.placeholder:
        placeholders = [candidate_query.placeholder.symbol]
        placeholders_values = [
            candidate_query.placeholder.values.all().values_list("value", flat=True)
        ]

    generated_queries = []
    placeholders_tuples = list(product(*placeholders_values))

    next_query = filtered_sparql
    for placeholders_tuple in placeholders_tuples:
        for i, placeholder in enumerate(placeholders):
            next_query = filtered_sparql.replace(placeholder, placeholders_tuple[i])
        generated_queries.append(next_query)


    out = None
    for q in generated_queries:
        result = execute_query(q)
        data = result["results"]["bindings"]
        if len(data) > 0:
            out = data[0]
    
    if out is None:
        return None

    monument_data = format_monument(out)

    return monument_data