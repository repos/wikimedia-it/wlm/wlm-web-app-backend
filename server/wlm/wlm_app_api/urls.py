from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import (
    MonumentAppViewSet,
    ClusterMonumentsApi,
    CategoriesDomainApi,
    UploadImageView,
    PersonalContributionsView,
    GeoContextViewSet,
    TranslationsView,
    LanguageViewSet,
)

router = DefaultRouter()
router.register(r"geo-contexts", GeoContextViewSet, basename="geo-contexts")
router.register(r"monuments", MonumentAppViewSet, basename="monuments")
router.register(r"languages", LanguageViewSet, basename="languages")


urlpatterns = router.urls

urlpatterns += [
    path("cluster-monuments/", ClusterMonumentsApi.as_view(), name="cluster_monuments"),
    path("categories-domain/", CategoriesDomainApi.as_view(), name="categories_domain"),
    path("upload-images/", UploadImageView.as_view(), name="upload_image"),
    path(
        "personal-contributions/",
        PersonalContributionsView.as_view(),
        name="personal_contributions",
    ),
    path("translations/", TranslationsView.as_view(), name="translations"),
]
