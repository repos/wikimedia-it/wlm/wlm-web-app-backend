from django.core.management.base import BaseCommand, CommandError
from wlm_app_api.helpers import get_upload_categories
from main.models import Monument




class Command(BaseCommand):
    """
    management command that wraps the get_upload_categories helper
    
    """ 
    def add_arguments(self, parser):
        parser.add_argument('q_number', type=str)
        
    def handle(self, *args, **options):
        q_number = options['q_number']
        monument = Monument.objects.get(q_number=q_number)
        categories = get_upload_categories(monument)
        print(categories)

        
