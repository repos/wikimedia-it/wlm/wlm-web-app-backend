from django.apps import AppConfig


class WlmAppApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'wlm_app_api'
