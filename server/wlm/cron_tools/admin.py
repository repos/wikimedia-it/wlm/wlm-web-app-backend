from django.contrib import admin
from .models import Job
from django.conf import settings
from django_ace import AceWidget
from django import forms


JOB_TYPE_CHOICES = [
    ("", "--------"),
]
for k in sorted(list(settings.CRON_JOB_HANLDERS.keys())):
    if type(settings.CRON_JOB_HANLDERS[k]) == str:
        label = k
    else:
        label = settings.CRON_JOB_HANLDERS[k].get("label", k)
    JOB_TYPE_CHOICES.append((k, label))
JOB_TYPE_CHOICES.sort(key=lambda x: x[1])


class JobAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["job_type"].widget = forms.Select(choices=JOB_TYPE_CHOICES)

    class Meta:
        model = Job
        widgets = {
            "kwargs": AceWidget(
                mode="json", theme="mono_industrial", width="50%", height="400px"
            ),
        }
        fields = "__all__"


class JobAdmin(admin.ModelAdmin):
    form = JobAdminForm
    

admin.site.register(Job, JobAdmin)

