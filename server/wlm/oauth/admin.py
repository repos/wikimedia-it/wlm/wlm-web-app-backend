from django.contrib import admin
from oauth.models import OAuth2Token

admin.site.register(OAuth2Token)