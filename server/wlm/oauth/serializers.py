from rest_framework import serializers
from django.contrib.auth.models import User
from main.models import GeoContextAdmin
from oauth.models import OAuth2Token
from  authlib.integrations.requests_client import OAuth2Auth
from django.conf import settings
import requests



class UserSerializer(serializers.ModelSerializer):

    admin_geo_contexts = serializers.SerializerMethodField()
    wlm_email_verified = serializers.SerializerMethodField()

    def get_admin_geo_contexts(self, obj):
        return [x.geo_context.id for x in GeoContextAdmin.objects.filter(user=obj)]
    
    def get_wlm_email_verified(self, obj):
        try:
            token = OAuth2Token.objects.get(user=obj, name="mediawiki")
            auth = OAuth2Auth(token.to_token())
            profile_url = settings.URL_AUTH_API+"/resource/profile"
            resp = requests.get(profile_url, auth=auth)
            profile = resp.json()
            return profile.get("email_verified", False)
        except:
            return False

    class Meta:
        model = User
        fields = ('id', 'username', 'admin_geo_contexts', 'wlm_email_verified')

class TokenSerializer(serializers.Serializer):
    refresh = serializers.CharField()
    access = serializers.CharField()


class RedeemInputSerializer(serializers.Serializer):
    redeem_token = serializers.CharField()